<?php

namespace InscopeRest\Processor;

use InscopeRest\Converter\Populator\Populator;
use InscopeRest\Modifier\Manager;

class DefaultPopulatorFactory implements PopulatorFactoryInterface
{
    /**
     * @param array $options
     * @param Manager $modifierManager
     * @return Populator
     */
    public function create(array $options = [], Manager $modifierManager = null) : Populator
    {
        $populator = new Populator($options);

        if ($modifierManager) {
            $populator->setModifierManager($modifierManager);
        }

        return $populator;
    }
}