<?php

namespace InscopeRest\Processor;

use InscopeRest\Modifier\ModifierProviderTrait;
use InscopeRest\Processor\Fetchers\DefaultFetcher;
use InscopeRest\Validation\Binder;
use InscopeRest\Validation\ErrorsThrowableCollection;
use InscopeRest\Validation\Performer;
use InscopeRest\Validation\Source\ArraySourceHandler;
use Illuminate\Contracts\Container\Container;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class AbstractProcessor
{
    use ModifierProviderTrait;

    /**
     * Keeps cached validation errors
     * @var array
     */
    private $errors;

    /**
     * @var array
     */
    private $data;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var Request
     */
    private $request;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return Request
     */
    protected function getRequest() : Request
    {
        if ($this->request === null) {
            $this->request = $this->container->make(Request::class);
        }

        return $this->request;
    }

    /**
     * @param Binder $binder
     */
    protected function rules(Binder $binder) : void
    {
        //
    }

    /**
     * List of request's allowable fields for a populate object
     * @return array
     */
    protected function allowable() : array
    {
        return [];
    }

    /**
     * Gets the prepared value by the key
     * @param string|array $key
     * @param null|mixed $default
     * @param null|string $modifiers
     * @param bool $validator
     * @return mixed
     * @throws \RuntimeException
     */
    public function get($key, $default = null, $modifiers = null, bool $validator = false)
    {
        if (is_array($key)) {
            $key = $this->findKey($key);
        }

        $value = $default;

        if ($this->has($key)) {

            if (!$validator || $this->isValid($key)) {
                $value = array_get($this->toArray(), $key);
            }
        }

        return $modifiers ? $this->modify($value, $modifiers) : $value;
    }

    /**
     * Checks whether the field is valid
     *
     * @param string $key
     * @return bool
     */
    private function isValid(string $key) : bool
    {
        return !isset($this->validate()[$key]);
    }

    /**
     * Modifiers the provided value
     * @param $value
     * @param array|string $modifiers
     * @return mixed
     */
    protected function modify($value, $modifiers)
    {
        return $this->getModifierManager()->modify($value, $modifiers);
    }

    /**
     * Tries to get a value by key with validation applied
     * @param string $key
     * @param $default
     * @param null $modifiers
     * @return mixed
     */
    public function tryGet(string $key, $default = null, $modifiers = null)
    {
        return $this->get($key, $default, $modifiers, true);
    }

    /**
     * Searches a key
     * @param array $keys
     * @return mixed
     */
    private function findKey(array $keys)
    {
        foreach ($keys as $key) {
            if ($this->has($key)) {
                return $key;
            }
        }

        return reset($keys);
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        if ($this->data === null) {
            $this->data = (new DefaultFetcher())->fetch($this->getRequest());
        }

        return $this->data;
    }

    /**
     * Shortcut method to populate the provided object with the provided data
     *
     * @param object $object
     * @param array $options
     * @return object
     * @throws BadRequestHttpException
     */
    protected function populate($object, array $options = [])
    {
        $data = $this->toArray();

        $data = $this->filterByAllowable($data);

        if (!$data) {
            throw new BadRequestHttpException('There\'s no allowable data in the body.');
        }

        $namespace = array_take($options, 'namespace');

        if ($namespace){
            $data = array_get($data, $namespace, []);
        }

        unset($options['namespace']);

        return $this->performPopulation($object, $data, $options);
    }

    /**
     * @return array
     */
    public function getFieldsWithNulls() : array
    {
        $data = $this->filterByAllowable($this->toArray());
        return array_keys(array_filter(array_dot($data), function($value){ return $value === null;}));
    }

    /**
     * @param object $object
     * @param array $data
     * @param array $options
     * @return object
     * @throws \InscopeRest\Converter\Populator\PopulatorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    protected function performPopulation($object, array $data, array $options = [])
    {
        /**
         * @var PopulatorFactoryInterface $factory
         */
        $factory = $this->container->make(PopulatorFactoryInterface::class);

        return $factory
            ->create($options, $this->getModifierManager())
            ->populate($data, $object);
    }

    /**
     * Filters data according to the validation rules
     *
     * @param array $data
     * @return array
     */
    protected function filterByRules(array $data) : array
    {
        $filtered = [];

        foreach (array_paths($data) as $path) {

            if (!$this->isValid($path)) {
                continue;
            }

            array_set($filtered, $path, array_get($data, $path));
        }

        return $filtered;
    }

    /**
     * Filters data according to the allowable fields
     *
     * @param array $data
     * @return array
     */
    protected function filterByAllowable(array $data) : array
    {
        $filtered = [];
        $fields = $this->allowable();

        foreach ($fields as $field) {

            if (!array_has($data, $field)) {
                continue;
            }

            array_set($filtered, $field, array_get($data, $field));
        }

        return $filtered;
    }

    /**
     * @return ErrorsThrowableCollection
     */
    public function validate() : ErrorsThrowableCollection
    {
        if ($this->errors === null) {

            $binder = new Binder();
            $this->rules($binder);

            $this->errors = (new Performer())->perform($binder, new ArraySourceHandler($this->toArray()));
        }

        return $this->errors;
    }

    /**
     * Indicates whether auto validation is allowed
     *
     * @return bool
     */
    public function validateAutomatically() : bool
    {
        return true;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key) : bool
    {
        return array_has($this->toArray(), $key);
    }
}