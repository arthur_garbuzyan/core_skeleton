<?php

namespace InscopeRest\Processor;

use Illuminate\Support\ServiceProvider as Provider;

class ServiceProvider extends Provider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() : void
    {
        $this->app->singleton(PopulatorFactoryInterface::class, DefaultPopulatorFactory::class);
    }

    public function boot() :  void
    {
        $this->app->afterResolving(
            AbstractProcessor::class,
            function (AbstractProcessor $processor) {
                if (!$processor->validateAutomatically()) {
                    return ;
                }

                $errors = $processor->validate();

                if (count($errors) > 0) {
                    throw $errors;
                }
            }
        );
    }
}