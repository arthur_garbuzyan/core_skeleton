<?php

namespace InscopeRest\Processor;

class SharedModifiers
{
    /**
     * @param $value
     * @return int|null
     */
    public function int($value) : ?int
    {
        return (int) $value;
    }

    /**
     * @param $value
     * @return bool|null
     */
    public function bool($value) : ?bool
    {
        if (is_string($value)) {
            $value = strtolower($value);
        }

        if (in_array($value, [0, '0', 'false'], true)) {
            return false;
        }

        if (in_array($value, [1, '1', 'true'], true)) {
            return true;
        }

        return !!$value;
    }

    /**
     * @param $value
     * @return float|null
     */
    public function float($value) : ?float
    {
        return (float) $value;
    }

    /**
     * @param string $value
     * @return array
     */
    public function explode($value) : array
    {
        $value = '' . $value;

        if ($value) {
            return explode(',', $value);
        }

        return [];
    }
}