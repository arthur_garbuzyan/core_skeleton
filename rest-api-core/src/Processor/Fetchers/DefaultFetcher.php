<?php

namespace InscopeRest\Processor\Fetchers;

use Illuminate\Http\Request;

class DefaultFetcher implements FetcherInterface
{
    /**
     * @param Request $request
     * @return array
     */
    public function fetch(Request $request) : array
    {
        if ($request->isMethod(Request::METHOD_DELETE)
            || $request->isMethod(Request::METHOD_GET)
        ) {
            return $request->query->all();
        }

        $all = array_replace_recursive(
            $request->isJson() ? $request->json()->all() : $request->request->all(),
            $request->files->all()
        );

        return ksort_recursive($all);
    }
}