<?php

namespace InscopeRest\Processor\Fetchers;

use Illuminate\Http\Request;

interface FetcherInterface
{
    /**
     * @param Request $request
     * @return array
     */
    public function fetch(Request $request) : array;
}