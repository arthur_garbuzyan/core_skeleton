<?php

namespace InscopeRest\Verifier;

use Illuminate\Support\ServiceProvider as Provider;

/**
 * The service provider executes the verification method for each action method unless an action is ignored
 */
class ServiceProvider extends Provider
{
    public function boot() : void
    {
        $this->app->resolving(ActionVerifiableInterface::class, function (ActionVerifiableInterface $controller) {
            $controller->middleware(Middleware::class);
        });
    }
}