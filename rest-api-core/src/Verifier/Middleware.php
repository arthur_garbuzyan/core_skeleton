<?php

namespace InscopeRest\Verifier;

use InscopeRest\Shared\Traits\ContainerAwareConstructorTrait;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use ReflectionMethod;
use RuntimeException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Middleware
{
    use ContainerAwareConstructorTrait;

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws \ReflectionException
     */
    public function handle(Request $request, Closure $next)
    {
        /**
         * @var Route $route
         */
        $route = $request->route();

        list($controller, $action) = explode('@', $route->getAction()['uses']);

        $args = array_values($route->parameters());

        if (!$args){
            return $next($request);
        }

        $method = 'verifyAction';

        if (!method_exists($controller, $method)){
            throw new RuntimeException('The "'.$method.'" method is missing.');
        }

        $reflection = new ReflectionMethod($controller, $method);

        if (!$reflection->isStatic()){
            throw new RuntimeException('The "'.$method.'" method must be static.');
        }

        foreach ($reflection->getParameters() as $index => $argument){

            $class = object_take($argument, 'class.name');

            if (!$class){
                continue ;
            }

            if ($class === Action::class || is_subclass_of($class, Action::class)){
                $instance = new Action($action);
            } else {
                $instance = $this->container->make($class);
            }

            array_splice(
                $args, $index, 0, [$instance]
            );
        }

        if (!call_user_func_array($controller.'::'.$method, $args)) {
            throw new NotFoundHttpException();
        }

        return $next($request);
    }
}