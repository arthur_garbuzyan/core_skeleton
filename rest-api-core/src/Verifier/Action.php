<?php

namespace InscopeRest\Verifier;

class Action
{
    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string|string[] $names
     * @return bool
     */
    public function is($names) : bool
    {
        if (!is_array($names)){
            $names = [$names];
        }

        return in_array($this->name, $names);
    }
}