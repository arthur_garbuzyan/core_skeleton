<?php

namespace InscopeRest\Iterators;

use Iterator;
use ArrayIterator;
use IteratorAggregate;
use RuntimeException;

class SubIteratorIterator implements Iterator
{
    /**
     * @var Iterator
     */
    private $iterator;

    /**
     * @var Iterator
     */
    private $subIterator;

    /**
     * @var callable
     */
    private $factory;

    /**
     * @var int
     */
    private $counter;

    /**
     * @param Iterator|IteratorAggregate|array $traversable
     * @param callable $factory
     */
    public function __construct($traversable, callable $factory = null)
    {
        $traversable = $this->resolveIterator($traversable);

        $this->iterator = $traversable;
        $this->factory = $factory;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->subIterator->current();
    }

    protected function nextIterator(): void
    {
        $this->subIterator = $this->findNextSubIterator();

    }

    public function next(): void
    {
        $this->counter++;

        $this->subIterator->next();

        if ($this->subIterator->valid()) {
            return;
        }

        $this->subIterator = $this->findNextSubIterator();
    }

    /**
     * @return string|int
     */
    public function key()
    {
        return $this->counter;
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return $this->iterator->valid() || $this->subIterator->valid();
    }

    public function rewind(): void
    {
        $this->counter = 0;
        $this->subIterator = $this->findNextSubIterator(true);
    }

    /**
     * @return Iterator
     */
    protected function getSubIterator(): Iterator
    {
        return $this->subIterator;
    }

    /**
     * @return Iterator
     */
    protected function getIterator(): Iterator
    {
        return $this->iterator;
    }

    /**
     * @return Iterator
     */
    private function buildSubIterator(): Iterator
    {
        if (is_null($this->factory)) {
            $traversable = $this->iterator->current();
        } else {
            $traversable = call_user_func($this->factory, $this->iterator->current());
        }

        return $this->resolveIterator($traversable);
    }

    /**
     * @param bool $isRewind
     * @return ArrayIterator|Iterator
     */
    private function findNextSubIterator(bool $isRewind = false): Iterator
    {
        if ($isRewind) {
            $this->iterator->rewind();
        } else {
            $this->iterator->next();
        }

        if (!$this->iterator->valid()) {
            return new ArrayIterator();
        }

        $subIterator = $this->buildSubIterator();
        $subIterator->rewind();

        if (!$subIterator->valid()) {
            return $this->findNextSubIterator();
        }

        return $subIterator;
    }

    /**
     * @param $traversable
     * @return Iterator
     * @throws RuntimeException
     */
    private function resolveIterator($traversable): Iterator
    {
        if (is_array($traversable)){
            return new ArrayIterator($traversable);
        }

        if ($traversable instanceof IteratorAggregate){
            return $traversable->getIterator();
        }

        if ($traversable instanceof Iterator){
            return $traversable;
        }

        throw new RuntimeException('Traversable must be an array or an instance of either Iterator or IteratorAggregate');
    }
}