<?php

namespace InscopeRest\Iterators;

use ArrayIterator;
use Illuminate\Support\Collection;
use IteratorAggregate;
use Traversable;

class PriorityQueue implements IteratorAggregate
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * @var array
     */
    private $sortedData;

    /**
     * @param mixed $data
     * @param int $priority
     */
    public function insert($data, int $priority) : void
    {
        $this->sortedData = null;

        $this->data[] = [
            'priority' => $priority,
            'data' => $data
        ];
    }

    /**
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator() : iterable
    {
        return new ArrayIterator($this->getSortedData());
    }

    /**
     * @return array
     */
    private function getSortedData() : array
    {
        if ($this->sortedData === null){
            $this->sortedData = (new Collection($this->data))
                ->sort()->map(function ($item) {
                    return $item['data'];
                })->toArray();
        }

        return $this->sortedData;
    }
}