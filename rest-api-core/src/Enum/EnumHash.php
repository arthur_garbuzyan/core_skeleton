<?php

namespace InscopeRest\Enum;

abstract class EnumHash
{
    use EnumVerifierTrait;

    /**
     * @var array
     */
    private $hash = [];

    /**
     * @param Enum $key
     * @param mixed $value
     * @return self
     */
    public function set(Enum $key, $value) : self
    {
        $this->verifyEnum($key);
        $this->hash[$key->value()] = $value;

        return $this;
    }

    /**
     * @param Enum $key
     * @param mixed $default
     * @return mixed
     */
    public function get(Enum $key, $default = null)
    {
        return $this->has($key) ? $this->hash[$key->value()] : $default;
    }

    /**
     * @param Enum $key
     * @return bool
     */
    public function has(Enum $key) : bool
    {
        $this->verifyEnum($key);
        return array_key_exists($key->value(), $this->hash);
    }

    /**
     * @return Enum[]
     */
    public function keys() : array
    {
        $keys = array_keys($this->hash);
        $objects = [];
        $enumClass = $this->getEnumClass();

        foreach ($keys as $key) {
            $objects[] = new $enumClass($key);
        }

        return $objects;
    }
}