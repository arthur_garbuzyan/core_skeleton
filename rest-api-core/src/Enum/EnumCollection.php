<?php

namespace InscopeRest\Enum;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;

abstract class EnumCollection implements ArrayAccess, Countable, IteratorAggregate
{
    use EnumVerifierTrait;

    /**
     * @var array|Enum[]
     */
    private $data = [];

    /**
     * @var array
     */
    private $simpleArray = null;

    /**
     * @param Enum[] $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $item) {
            $this->push($item);
        }
    }

    /**
     * @param array $data
     * @return self
     */
    public static function make(array $data) : self
    {
        $collection = new static();
        $class = $collection->getEnumClass();

        foreach ($data as $item) {
            $collection->push(new $class($item));
        }

        return $collection;
    }

    /**
     * @param Enum $item
     */
    public function push(Enum $item) : void
    {
        $this->verifyEnum($item);

        $this->data[] = $item;
        $this->simpleArray = null;
    }

    /**
     * @return Enum
     */
    public function pop() : Enum
    {
        $this->simpleArray = null;

        return array_pop($this->data);
    }

    /**
     * @param Enum $item
     * @return bool
     */
    public function has(Enum $item) : bool
    {
        $this->verifyEnum($item);

        return in_array($item->value(), $this->toArray());
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        if ($this->simpleArray === null) {
            $this->simpleArray = array_map(function (Enum $item) {
                return $item->value();
            }, $this->data);
        }

        return $this->simpleArray;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) : bool
    {
        return array_key_exists($offset, $this->data);
    }

    /**
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value) : void
    {
        $this->verifyEnum($value);

        $this->data[$offset] = $value;
        $this->simpleArray = null;
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset) : void
    {
        unset($this->data);
        $this->simpleArray = null;
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->data);
    }
}
