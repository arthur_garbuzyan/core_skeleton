<?php

namespace InscopeRest\Enum;

use RuntimeException;

trait EnumVerifierTrait
{
    /**
     * @return string
     */
    abstract static public function getEnumClass() : string;

    /**
     * @param Enum $key
     */
    private function verifyEnum(Enum $key) : void
    {
        $valueClass = get_class($key);
        $enumClass = $this->getEnumClass();

        if (!($key instanceof $enumClass) || !call_user_func([$enumClass, 'has'], $key->value())) {
            throw new RuntimeException("Key should be '{$enumClass}' but received '{$valueClass}'");
        }
    }
}
