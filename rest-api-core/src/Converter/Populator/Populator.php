<?php

namespace InscopeRest\Converter\Populator;

use InscopeRest\Converter\Populator\Resolvers\AbstractResolver;
use InscopeRest\Converter\Populator\Resolvers\AggregateResolver;
use InscopeRest\Converter\Populator\Resolvers\AllowResolver;
use InscopeRest\Converter\Populator\Resolvers\ArrayResolver;
use InscopeRest\Converter\Populator\Resolvers\BooleanResolver;
use InscopeRest\Converter\Populator\Resolvers\DateTimeResolver;
use InscopeRest\Converter\Populator\Resolvers\EnumCollectionResolver;
use InscopeRest\Converter\Populator\Resolvers\EnumHashResolver;
use InscopeRest\Converter\Populator\Resolvers\EnumResolver;
use InscopeRest\Converter\Populator\Resolvers\HintCollectionResolver;
use InscopeRest\Converter\Populator\Resolvers\HintResolver;
use InscopeRest\Converter\Populator\Resolvers\InstanceResolver;
use InscopeRest\Converter\Populator\Resolvers\NullResolver;
use InscopeRest\Converter\Populator\Resolvers\SimpleResolver;
use InscopeRest\Converter\Populator\Resolvers\StringResolver;
use InscopeRest\Converter\Populator\Resolvers\VectorResolver;
use InscopeRest\Converter\Support\ConfigProvider;
use InscopeRest\Converter\Support\DefaultTrait;
use InscopeRest\Converter\Support\IgnoreTrait;
use InscopeRest\Converter\Support\MapTrait;
use InscopeRest\Converter\Support\ModifyTrait;
use InscopeRest\Converter\Support\Scope;
use InscopeRest\Iterators\PriorityQueue;
use ReflectionClass;
use ReflectionFunction;
use ReflectionFunctionAbstract;
use ReflectionMethod;

class Populator
{
    use DefaultTrait;
    use IgnoreTrait;
    use MapTrait;
    use ModifyTrait;

    /**
     * The current scope
     *
     * @var Scope
     */
    protected $scope;

    /**
     * @var PriorityQueue|AbstractResolver[]
     */
    protected $resolvers;

    /**
     * @var ConfigProvider
     */
    protected $config;

    /**
     * @param array $config has keys:
     * - 'ignore' - list of ignored fields
     * - 'allow' - list of fields that can take array
     * - 'hint' - hash of fields/class that can take array of class
     * - 'resolver' - list of resolvers for fields
     */
    public function __construct($config = [])
    {
        $this->scope = new Scope();
        $this->config = new ConfigProvider(array_diff_key($config,
            array_flip([AllowResolver::CONFIG_KEY, HintResolver::CONFIG_KEY, AggregateResolver::CONFIG_KEY])));

        $this->allow(array_take($config, AllowResolver::CONFIG_KEY, []));

        foreach (array_take($config, HintResolver::CONFIG_KEY, []) as $field => $className) {
            $this->hint($field, $className);
        }

        foreach (array_take($config, AggregateResolver::CONFIG_KEY, []) as $field => $className) {
            $this->resolver($field, $className);
        }

        $this->ignore(array_take($config, 'ignore', []));
        $this->map(array_take($config, 'map', []));
        $this->defaults(array_take($config, 'defaults', []));

        foreach (array_take($config, 'modify', []) as $type => $block) {
            foreach ($block as $field => $modifiers) {
                $this->modify($type, $field, $modifiers);
            }
        }

        $this->resolvers = new PriorityQueue();
        $this->createResolvers();
    }

    /**
     * @param AbstractResolver $resolver
     * @param int $priority
     */
    public function addGlobalResolver(AbstractResolver $resolver, int $priority = 0) : void
    {
        $resolver->setPopulator($this)->setConfig($this->config);
        $this->resolvers->insert($resolver, $priority);
    }

    /**
     * The method populates an object with data from array with resolvers/modifiers applied
     *
     * @param array $data
     * @param object $object
     * @return object - returns the same object as $object
     * @throws PopulatorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function populate(array $data, $object)
    {
        $objectReflection = new ReflectionClass($object);

        $data = $this->exchangeDefaults($data);

        foreach ($data as $field => $value) {
            $method = $this->tryGetSetterName($data, $field, $objectReflection);

            if (!$method) {
                continue;
            }

            $this->scope->create($field);
            $scopeField = $this->scope->get();

            $getter = $this->getFieldName($field);
            $oldValue = $objectReflection->hasMethod(make_getter($getter)) ?
                object_take($object, $getter) : null;

            $value = $this->getValue($data, $field);

            $value = $this->modifyBefore($value, $scopeField);

            $value = $this->resolve($value, $method, $oldValue);

            $value = $this->modifyAfter($value, $scopeField);

            if ($this->isArgumentNullable($method) || $value !== null) {
                $this->invoke($method, $value, $object);
            }

            $this->scope->drop();
        }

        return $object;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function exchangeDefaults(array $data) : array
    {
        foreach ($this->defaults as $key => $value){
            $defaultValue = array_get($data, $key);

            if ($defaultValue === null){
                array_set($data, $key, $value);
            }
        }

        $this->defaults = [];

        return $data;
    }

    /**
     * @param array $data
     * @param string $field
     * @return mixed
     */
    protected function getValue(array $data, string $field)
    {
        return array_take($data, $field);
    }

    /**
     * @param array $data
     * @param string $field
     * @return bool
     */
    protected function hasValue(array $data, string $field) : bool
    {
        return array_key_exists($field, $data);
    }

    /**
     * Creates field name from the setter reflection
     *
     * @param string $field
     * @return string
     */
    protected function getFieldName(string $field) : string
    {
        return $this->getMappedName($this->scope->get($field), $field);
    }

    /**
     * Checks whether the setter can be processed
     *
     * @param array $data
     * @param string $field
     * @param ReflectionClass $objectReflection
     * @return ReflectionMethod|null
     */
    protected function tryGetSetterName(array $data, string $field, ReflectionClass $objectReflection) : ?ReflectionMethod
    {
        $methodName = make_setter($this->getFieldName($field));

        if (!$objectReflection->hasMethod($methodName)) {
            return null;
        }

        $setter = $objectReflection->getMethod($methodName);

        if (!$setter->isPublic()) {
            return null;
        }

        if (!is_setter($setter->name)) {
            return null;
        }

        if (!$this->hasValue($data, $field)) {
            return null;
        }

        if ($this->isIgnored($this->scope->get($field))) {
            return null;
        }

        if (count($setter->getParameters()) != 1) {
            return null;
        }

        return $setter;
    }

    /**
     * Resolves the value
     *
     * @param mixed $value
     * @param ReflectionFunctionAbstract $setter
     * @param $oldValue
     * @return mixed
     * @throws PopulatorException
     */
    protected function resolve($value, ReflectionFunctionAbstract $setter, $oldValue)
    {
        $resolvers = $this->resolvers;

        $scope = $this->scope->get();
        $parameter = $setter->getParameters()[0];

        foreach ($resolvers as $resolver) {
            $canResolve = $resolver->canResolve($scope, $value, $parameter);

            if ($canResolve) {
                return $resolver->resolve($scope, $value, $oldValue, $parameter);
            }
        }

        throw new PopulatorException('Unable to resolve value for "'.$scope.'".');
    }

    /**
     * @param ReflectionFunctionAbstract $setter
     * @return bool
     */
    protected function isArgumentNullable(ReflectionFunctionAbstract $setter) : bool
    {
        $argument = $setter->getParameters()[0];
        $isDefaultNull =$argument->isDefaultValueAvailable() &&  $argument->getDefaultValue() === null;
        $isClass = $argument->getClass() !== null;
        $isArray = $argument->isArray();

        if ($isArray && !$isDefaultNull) {
            return false;
        }

        if ($isClass && !$isDefaultNull) {
            return false;
        }

        return true;
    }

    /**
     * Creates a list of resolvers
     */
    protected function createResolvers() : void
    {
        $this->addGlobalResolver(new NullResolver(), PHP_INT_MIN + 10);
        $this->addGlobalResolver(new SimpleResolver(), PHP_INT_MAX - 10);

        $resolvers = [
            new EnumCollectionResolver(),
            new AggregateResolver(),
            new EnumHashResolver(),
            new HintResolver(),
            new HintCollectionResolver(),
            new AllowResolver(),
            new DateTimeResolver(),
            new EnumResolver(),
            new InstanceResolver(),
            new VectorResolver(),
            new ArrayResolver(),
            new BooleanResolver(),
            new StringResolver(),
        ];

        foreach ($resolvers as $i => $resolver) {
            $this->addGlobalResolver($resolver, 10 + ($i * 10));
        }
    }

    /**
     * @param ReflectionMethod|ReflectionFunction $reflectionFunction
     * @param $value
     * @param $object
     */
    protected function invoke($reflectionFunction, $value, $object) : void
    {
        if ($reflectionFunction instanceof ReflectionMethod) {
            $reflectionFunction->invoke($object, $value);
        } else if ($reflectionFunction instanceof ReflectionFunction) {
            $reflectionFunction->invoke($value);
        }
    }

    /**
     * Sets list allowed fields that contains arrays
     * @param array $fields
     * @return self
     */
    public function allow(array $fields) : self
    {
        $allows = $this->config->get(AllowResolver::CONFIG_KEY, []);
        $this->config->set(AllowResolver::CONFIG_KEY, array_merge($allows, $fields));

        return $this;
    }

    /**
     * Gives hint about array type to populator
     * @param string $field
     * @param $className
     * @return self
     */
    public function hint(string $field, string $className) : self
    {
        $this->config->set(HintResolver::CONFIG_KEY.'.'.$field, $className);
        return $this;
    }

    /**
     * Gives to add resolver for selected field
     * @param $field
     * @param callable $callback
     * @return self
     */
    public function resolver(string $field, callable $callback) : self
    {
        $this->config->set(AggregateResolver::CONFIG_KEY.'.'.$field, $callback);
        return $this;
    }
}