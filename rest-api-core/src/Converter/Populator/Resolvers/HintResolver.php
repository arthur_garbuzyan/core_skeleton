<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

/**
 * Creates an object of class that is specified in the config for specific fields
 */
class HintResolver extends AbstractResolver
{
    const CONFIG_KEY = 'hint';

    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return object
     * @throws \InscopeRest\Converter\Populator\PopulatorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter)
    {
        $className = $this->config->get(self::CONFIG_KEY . ".{$field}");

        return $this->populator->populate($value, new $className());
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $hint = $this->config->get(self::CONFIG_KEY . ".{$field}", '');

        return $hint && is_string($hint) && !starts_with($hint, 'collection:');
    }
}