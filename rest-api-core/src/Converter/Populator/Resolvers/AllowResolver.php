<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

/**
 * The resolver passes true a value for a specified fields.
 * That is useful, when you want to specify explicitly to what fields the value from the dataset must be just passed
 */
class AllowResolver extends AbstractResolver
{
    const CONFIG_KEY = 'allow';

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        return in_array($field, $this->config->get(self::CONFIG_KEY, []));
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return mixed
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter)
    {
        return $value;
    }
}