<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use DateTime;
use DateTimeZone;
use ReflectionParameter;

class DateTimeResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return DateTime
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : DateTime
    {
        /**
         * @var DateTime $datetime
         */
        $datetime = $parameter->getClass()->newInstance($value);

        $datetime->setTimezone(new DateTimeZone('UTC'));

        return $datetime;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $class = $parameter->getClass();

        return $class && ($class->isSubclassOf(DateTime::class) || $class->getName() === DateTime::class);
    }
}