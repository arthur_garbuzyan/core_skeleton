<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use InscopeRest\Enum\EnumCollection;
use ReflectionParameter;

class EnumCollectionResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $class = $parameter->getClass();

        return $class && $class->isSubclassOf(EnumCollection::class);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param array $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return EnumCollection
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : EnumCollection
    {
        return call_user_func([$parameter->getClass()->getName(), 'make'], $value);
    }
}