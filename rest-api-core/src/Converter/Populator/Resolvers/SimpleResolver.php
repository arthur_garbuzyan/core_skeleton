<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

class SimpleResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return mixed
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter)
    {
        return $value;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        return $parameter->getClass() === null && !is_array($value);
    }
}