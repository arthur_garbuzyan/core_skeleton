<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

class InstanceResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $class = $parameter->getClass();
        return $class && $class->isInstantiable() && is_array($value);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return object
     * @throws \InscopeRest\Converter\Populator\PopulatorException
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter)
    {
        if ($oldValue === null) {
            $oldValue = $parameter->getClass()->newInstance();
        }

        return $this->populator->populate($value, $oldValue);
    }
}