<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

class NullResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        return $value === null;
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return null
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter)
    {
        return null;
    }
}