<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use InscopeRest\Converter\Populator\Populator;
use InscopeRest\Converter\Support\ConfigProvider;
use ReflectionParameter;

abstract class AbstractResolver
{
    /**
     * @var Populator
     */
    protected $populator;

    /**
     * @var ConfigProvider
     */
    protected $config;

    /**
     * @param Populator $populator
     * @return self
     */
    public function setPopulator(Populator $populator) : self
    {
        $this->populator = $populator;
        return $this;
    }

    /**
     * @param ConfigProvider $config
     * @return self
     */
    public function setConfig(ConfigProvider $config) : self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    abstract public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool;

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return mixed
     */
    abstract public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter);
}