<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use InscopeRest\Validation\Rules\BooleanCast;
use ReflectionParameter;

class BooleanResolver extends AbstractResolver
{
    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return mixed
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter)
    {
        if ($value === 'false') {
            return false;
        }

        if ($value === 'true') {
            return true;
        }

        return $value;
    }

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter): bool
    {
        return (new BooleanCast(true))->check($value) === null;
    }
}