<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

class StringResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return mixed
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : string
    {
        return (string) $value;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $type = $parameter->getType();
        return $type && $type->getName() === 'string' && $type->isBuiltin() && !is_array($value) && (
            (!is_object($value) && settype($value, 'string') !== false)
            || is_object($value) && method_exists($value, '__toString')
        );
    }
}