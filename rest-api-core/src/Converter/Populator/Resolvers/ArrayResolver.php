<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

class ArrayResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        return is_array($value) && $parameter->isArray();
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return array
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : array
    {
        return $value;
    }
}