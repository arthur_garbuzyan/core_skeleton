<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

class AggregateResolver extends AbstractResolver
{
    const CONFIG_KEY = 'resolver';

    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return callable
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : callable
    {
        $callable = $this->config->get(self::CONFIG_KEY . ".{$field}");

        /**
         * @var callable $callable
         */
        return $callable($this->populator, $value);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $resolver = $this->config->get(self::CONFIG_KEY . ".{$field}");

        return $resolver && is_callable($resolver);
    }
}