<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use InscopeRest\Enum\Enum;
use ReflectionParameter;

class EnumResolver extends AbstractResolver
{
    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return Enum
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : Enum
    {
        return $parameter->getClass()->newInstance($value);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $class = $parameter->getClass();

        return $class && $class->isSubclassOf(Enum::class);
    }
}