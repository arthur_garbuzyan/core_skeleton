<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use InscopeRest\Enum\EnumHash;
use ReflectionParameter;

class EnumHashResolver extends AbstractResolver
{
    const CONFIG_KEY = 'hint';

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $class = $parameter->getClass();

        return $class && $class->isSubclassOf(EnumHash::class);
    }

    /**
     * Resolves a value
     *
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return EnumHash
     * @throws \InscopeRest\Converter\Populator\PopulatorException
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : EnumHash
    {
        /**
         * @var EnumHash $hash
         */
        $hash = $parameter->getClass()->newInstance();
        $enumName = $hash->getEnumClass();
        $className = $this->config->get(self::CONFIG_KEY . ".{$field}");

        foreach ($value as $key => $item) {
            $item = $className === null ? $item : $this->populator->populate($item, new $className());
            $hash->set(new $enumName($key), $item);
        }

        return $hash;
    }
}