<?php

namespace InscopeRest\Converter\Populator\Resolvers;

use ReflectionParameter;

class HintCollectionResolver extends AbstractResolver
{
    const CONFIG_KEY = 'hint';
    const TOKEN = 'collection:';

    /**
     * @param string $field
     * @param mixed $value
     * @param mixed $oldValue
     * @param ReflectionParameter $parameter
     * @return array
     * @throws \InscopeRest\Converter\Populator\PopulatorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function resolve(string $field, $value, $oldValue, ReflectionParameter $parameter) : array
    {
        $className = str_replace(self::TOKEN, '', $this->config->get(self::CONFIG_KEY . ".{$field}"));
        $array = [];

        foreach ($value as $item) {
            $array[] = $this->populator->populate($item, new $className());
        }

        return $array;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param ReflectionParameter $parameter
     * @return bool
     */
    public function canResolve(string $field, $value, ReflectionParameter $parameter) : bool
    {
        $hint = $this->config->get(self::CONFIG_KEY . ".{$field}", '');

        return $hint && is_string($hint) && starts_with($hint, self::TOKEN);
    }
}