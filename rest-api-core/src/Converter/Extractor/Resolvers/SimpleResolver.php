<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;

class SimpleResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        return true;
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return mixed
     */
    public function resolve(string $scope, $value, Root $root = null)
    {
        return $value;
    }
}