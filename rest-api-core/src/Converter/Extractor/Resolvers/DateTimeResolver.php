<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;
use DateTime;

class DateTimeResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        return $value instanceof DateTime;
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param DateTime $value
     * @param Root $root
     * @return string
     */
    public function resolve(string $scope, $value, Root $root = null) : string
    {
        $modifierManager = $this->extractor->getModifierManager();

        return $modifierManager ? $modifierManager->modify($value, 'datetime') :
            $value->format('Y-m-d H:i:s');
    }
}