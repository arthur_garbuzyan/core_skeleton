<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;
use InscopeRest\Enum\EnumCollection;

class EnumCollectionResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        return $value instanceof EnumCollection;
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param EnumCollection $value
     * @param Root $root
     * @return string[]
     */
    public function resolve(string $scope, $value, Root $root = null) : array
    {
        return $value->toArray();
    }
}