<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;

class InstanceResolver extends AbstractResolver
{

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        return is_object($value);
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return mixed
     * @throws \InscopeRest\Converter\Extractor\ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function resolve(string $scope, $value, Root $root = null)
    {
        return $this->extractor->extract($value, $root);
    }
}