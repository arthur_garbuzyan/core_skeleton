<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;
use InscopeRest\Enum\Enum;

class EnumResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        return $value instanceof Enum;
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param Enum $value
     * @param Root $root
     * @return string
     */
    public function resolve(string $scope, $value, Root $root = null) : string
    {
        return $value->value();
    }
}