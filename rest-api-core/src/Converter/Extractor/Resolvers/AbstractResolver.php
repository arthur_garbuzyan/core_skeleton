<?php
namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Extractor;
use InscopeRest\Converter\Extractor\Root;

abstract class AbstractResolver
{
    /**
     * @var Extractor
     */
    protected $extractor;

    /**
     * @var array
     */
    protected $config = [];

    /**
	 * @param array $config
     */
    public function __construct(array $config = [])
    {
		$this->config = $config;
    }

	/**
	 * @param Extractor $extractor
	 */
	public function setExtractor(Extractor $extractor) : void
	{
		$this->extractor = $extractor;
	}

	/**
	 * @param array $config
	 */
	public function setConfig(array $config) : void
	{
		$this->config = $config;
	}

    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
	 * @param Root $root
     * @return bool
     */
    abstract public function canResolve(string $scope, $value, Root $root = null) : bool;

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param mixed $value
	 * @param Root $root
     * @return mixed
     */
    abstract public function resolve(string $scope, $value, Root $root = null);

    public function log()
    {
    }
}