<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;
use InscopeRest\Enum\EnumHash;

class EnumHashResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        return $value instanceof EnumHash;
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param EnumHash $value
     * @param Root $root
     * @return array
     * @throws \InscopeRest\Converter\Extractor\ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function resolve(string $scope, $value, Root $root = null) : array
    {
        $hash = [];
        $keys = $value->keys();

        foreach ($keys as $key) {
            $hash[$key->value()] = $this->extractor->extract($value->get($key), $root);
        }

        return $hash;
    }
}