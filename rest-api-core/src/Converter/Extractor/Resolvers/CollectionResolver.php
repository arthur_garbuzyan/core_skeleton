<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;
use Traversable;

class CollectionResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        return is_vector($value, false) || ($value instanceof Traversable);
    }

    /**
     * Resolves a value
     *
     * @param string $scope
     * @param array|Traversable $value
     * @param Root $root
     * @return array
     * @throws \InscopeRest\Converter\Extractor\ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    public function resolve(string $scope, $value, Root $root = null)
    {
        $array = [];

        foreach ($value as $item) {
            $array[] = $this->extractor->extract($item, $root);
        }

        return $array;
    }
}