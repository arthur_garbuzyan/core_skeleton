<?php

namespace InscopeRest\Converter\Extractor\Resolvers;

use InscopeRest\Converter\Extractor\Root;

class SpecificationResolver extends AbstractResolver
{
    /**
     * Checks whether the resolver can resolve a value
     *
     * @param string $scope
     * @param mixed $value
     * @param Root $root
     * @return bool
     */
    public function canResolve(string $scope, $value, Root $root = null) : bool
    {
        if ($root === null) {
            return false;
        }

        return (bool) $this->tryGetResolver(get_class($root->getObject()), $root->getKey());
    }

    /**
     * @param string $class
     * @param string $field
     * @return callable|null
     */
    private function tryGetResolver(string $class, string $field) : ?callable
    {
        foreach ($this->config as $target => $properties) {
            if ($class === $target || is_subclass_of($class, $target)) {
                return array_take($properties, $field);
            }
        }

        return null;
    }

    /**
     * @param string $scope
     * @param mixed $value
     * @param Root|null $root
     * @return mixed
     */
    public function resolve(string $scope, $value, Root $root = null)
    {
        $resolver = $this->tryGetResolver(get_class($root->getObject()), $root->getKey());

        return $resolver($value);
    }
}