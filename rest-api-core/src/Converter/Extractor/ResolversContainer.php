<?php

namespace InscopeRest\Converter\Extractor;

use InscopeRest\Converter\Extractor\Resolvers\AbstractResolver;

class ResolversContainer
{
    private $resolvers = [];

    /**
     * @param string $class
     * @return AbstractResolver
     */
    public function get(string $class) : AbstractResolver
    {
        if (!isset($this->resolvers[$class])) {

            $this->resolvers[$class] = new $class();
        }

        return $this->resolvers[$class];
    }
}