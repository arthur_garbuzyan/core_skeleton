<?php

namespace InscopeRest\Converter\Extractor;

use InscopeRest\Converter\Extractor\Resolvers\AbstractResolver;
use InscopeRest\Converter\Extractor\Resolvers\CollectionResolver;
use InscopeRest\Converter\Extractor\Resolvers\DateTimeResolver;
use InscopeRest\Converter\Extractor\Resolvers\EnumCollectionResolver;
use InscopeRest\Converter\Extractor\Resolvers\EnumHashResolver;
use InscopeRest\Converter\Extractor\Resolvers\EnumResolver;
use InscopeRest\Converter\Extractor\Resolvers\InstanceResolver;
use InscopeRest\Converter\Extractor\Resolvers\SimpleResolver;
use InscopeRest\Converter\Extractor\Resolvers\SpecificationResolver;
use InscopeRest\Converter\Support\IgnoreTrait;
use InscopeRest\Converter\Support\MapTrait;
use InscopeRest\Converter\Support\ModifyTrait;
use InscopeRest\Converter\Support\Scope;
use InscopeRest\Iterators\PriorityQueue;
use ReflectionClass;
use ReflectionMethod;

class Extractor
{
    use IgnoreTrait;
    use MapTrait;
    use ModifyTrait;

    /**
     * The current scope
     *
     * @var Scope
     */
    protected $scope;

    /**
     * @var array
     */
    protected $only = [];

    /**
     * @var array
     */
    protected $onlyByClass = [];

    /**
     * @var array
     */
    protected $ignoreByClass = [];

    /**
     * @var array
     */
    protected $force = [];

    /**
     * @var array
     */
    protected $calculatedProperties = [];

    /**
     * @var AbstractResolver[]
     */
    protected $resolvers;

    /**
     * @var ResolversContainer
     */
    protected $resolversContainer;

    /**
     * @var callable
     */
    protected $filterWithinContext;

    /**
     * @var array
     */
    protected $modifiersByClass = [];

    /**
     * @param array $config has keys:
     * - 'ignore' - list of ignored fields
     * - 'map' - list of aliases for fields
     * - 'only' - list of fields that should be returned after extract
     * - 'onlyByClass' - list of classes with fields that should be returned after extract
     */
    public function __construct(array $config = [])
    {
        $this->scope = new Scope();
        $this->resolvers = new PriorityQueue();
        $this->resolversContainer = new ResolversContainer();

        $this->ignore(array_take($config, 'ignore', []));
        $this->ignoreByClass(array_take($config, 'ignoreByClass', []));
        $this->force(array_take($config, 'force', []));
        $this->map(array_take($config, 'map', []));
        $this->only(array_take($config, 'only', []));
        $this->onlyByClass(array_take($config, 'onlyByClass', []));
        $this->setCalculatedProperties(array_take($config, 'calculatedProperties', []));
        $this->setSpecifications(array_take($config, 'specifications', []));

        if ($filter = array_take($config, 'filterWithinContext')){
            $this->setFilterWithinContext($filter);
        }

        foreach (array_take($config, 'modify', []) as $type => $block) {
            foreach ($block as $field => $modifiers) {
                $this->modify($type, $field, $modifiers);
            }
        }

        $this->modifiersByClass = array_take($config, 'modifyByClass', []);

        $this->createResolvers();
    }

    /**
     * For each method of $object starts with is*, get* and has* extract method checks type
     * of the variable that returned from a method.
     * If variable is class then method is called recursive for this object.
     * If variable is Enum then value derived from Enum added to array.
     * Elsewhere variable added to array.
     *
     * @param mixed $object
     * @param Root $root
     * @return array
     * @throws ExtractorException
     * @throws \ReflectionException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     */
    public function extract($object, Root $root = null) : array
    {
        return is_object($object) ? $this->extractObject($object, $root) : $this->extractValue($object, $root);
    }

    /**
     * @param object $object
     * @param Root $root = null
     * @return array
     * @throws ExtractorException
     * @throws \ReflectionException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     */
    protected function extractObject($object, Root $root = null) : array
    {
        $transformedObject = [];

        $objectReflection = new ReflectionClass($object);
        $class = get_class($object);

        foreach ($objectReflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            $key = $this->tryGetKey($method, $object, $root);

            if (!$key) {
                continue;
            }

            $this->scope->create($key);

            $value = $this->getValue($object, $method);

            $value = $this->extractValue($value, new Root($object, $key));

            $transformedObject[$this->getFieldName($key)] = $value;

            $this->scope->drop();
        }

        foreach ($this->calculatedProperties as $target => $options){

            if ($class !== $target && !is_subclass_of($class, $target)){
                continue ;
            }

            foreach ($options as $key => $callback) {

                //if target is interface check options on actual class
                if (interface_exists($target)) {
                    if (!$this->canProceedWithKey($key, $class)){
                        continue ;
                    }
                } else {
                    if (!$this->canProceedWithKey($key, $target)){
                        continue ;
                    }
                }


                if (!$this->isKeyAcceptableWithinContext($key, $object, $root)){
                    continue ;
                }

                $this->scope->create($key);

                $value = $callback($object);
                $value = $this->extractValue($value, new Root($object, $key));

                $transformedObject[$this->getFieldName($key)] = $value;
                $this->scope->drop();
            }
        }

        return $transformedObject;
    }

    /**
     * @param array $array
     * @param Root|null $root
     * @return array
     * @throws ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     */
    protected function extractArray(array $array, Root $root = null)
    {
        $transformedObject = [];

        foreach ($array as $key => $value) {
            $value = $this->extractValue($value, $root);

            $this->scope->create($key);

            $transformedObject[$key] = $value;

            $this->scope->drop();
        }

        return $transformedObject;
    }

    /**
     * @param mixed $value
     * @param Root $root
     * @return mixed
     * @throws ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     */
    protected function extractValue($value, Root $root = null)
    {
        if (is_array($value)) {
            return $this->extractArray($value, $root);
        }

        $path = $this->scope->get();

        if ($root !== null){
            $value = $this->modifyValueByClass('before', $value, $root);
        }

        $value = $this->modifyBefore($value, $path);

        $value = $this->resolve($value, $root);

        if ($root !== null){
            $value = $this->modifyValueByClass('after', $value, $root);
        }

        return $this->modifyAfter($value, $path);
    }

    /**
     * @param string $position
     * @param mixed $value
     * @param Root $root
     * @return mixed
     */
    protected function modifyValueByClass(string $position, $value, Root $root)
    {
        $class = get_class($root->getObject());
        $field = $root->getKey();

        if ($modifiers = array_get($this->searchModifiersByClass($class), $position.'.'.$field)){
            return $this->getModifierManager()->modify($value, $modifiers);
        }

        return $value;
    }

    /**
     * @param string $class
     * @return array
     */
    protected function searchModifiersByClass(string $class) : array
    {
        foreach ($this->modifiersByClass as $target => $config){
            if ($class === $target || is_subclass_of($class, $target)){
                return $config;
            }
        }

        return [];
    }

    /**
     * @param array $properties
     * @return Extractor
     */
    public function setCalculatedProperties(array $properties) : self
    {
        $this->calculatedProperties = $properties;
        return $this;
    }

    /**
     * @param array $specifications
     * @return Extractor
     */
    public function setSpecifications(array $specifications) : self
    {
        $this->resolversContainer->get(SpecificationResolver::class)->setConfig($specifications);
        return $this;
    }

    /**
     * @param callable $callback
     * @return Extractor
     */
    public function setFilterWithinContext(callable $callback) : self
    {
        $this->filterWithinContext = $callback;
        return $this;
    }

    /**
     * @param array $fields
     * @return Extractor
     */
    public function only(array $fields) : self
    {
        $this->only = $fields;
        return $this;
    }

    /**
     * @param array $specification
     * @return Extractor
     */
    public function onlyByClass(array $specification) : self
    {
        $this->onlyByClass = $specification;
        return $this;
    }

    /**
     * @param array $specification
     * @return Extractor
     */
    public function ignoreByClass(array $specification) : self
    {
        $this->ignoreByClass = $specification;
        return $this;
    }

    /**
     * @param array $fields
     * @return Extractor
     */
    public function force(array $fields) : self
    {
        $this->force = $fields;
        return $this;
    }

    /**
     * @param string $key
     * @param object $context
     * @param Root $root
     * @return bool
     */
    protected function isKeyAcceptableWithinContext(string $key, $context, Root $root = null) : bool
    {
        if ($this->filterWithinContext === null){
            return true;
        }

        return call_user_func($this->filterWithinContext, $key, $context, $root);
    }

    /**
     * @param ReflectionMethod $method
     * @param object $object
     * @param Root $root
     * @return null|string
     */
    protected function tryGetKey(ReflectionMethod $method, $object, Root $root = null) : ?string
    {
        if (!is_getter($method->name)) {
            return null;
        }

        if (count($method->getParameters()) != 0) {
            return null;
        }

        $key = make_property($method->name);

        if (!$this->canProceedWithKey($key, get_class($object))){
            return null;
        }

        if (!$this->isKeyAcceptableWithinContext($key, $object, $root)){
            return null;
        }

        return $key;
    }

    /**
     * @param string $key
     * @param string $class
     * @return bool
     */
    protected function canProceedWithKey(string $key, string $class) : bool
    {
        $fullKey = $this->scope->get($key);

        if (!$this->isAllowed($fullKey, $class)) {
            return false;
        }

        if ($this->isIgnored($fullKey)) {
            return false;
        }

        if ($this->isIgnoredByClass($fullKey, $class)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $fullKey
     * @param string $class
     * @return bool
     */
    protected function isIgnoredByClass(string $fullKey, string $class) : bool
    {
        $ignoredByClass = $this->getFieldsByClassWithParents($class, $this->ignoreByClass);
        $isIgnored = false;

        if (!in_array($fullKey, $this->force)) {
            $key = get_short_key($fullKey);

            foreach ($ignoredByClass as $field) {
                if ($key === $field) {
                    $isIgnored = true;

                    break;
                }
            }
        }

        return count($ignoredByClass) > 0 && $isIgnored;
    }

    /**
     * @param string $key
     * @param string $class
     * @return bool
     */
    protected function isAllowed(string $key, string $class) : bool
    {
        $isAllowed = false;

        $only = $this->only;

        foreach ($only as $field) {
            if ($key === $field || starts_with($field, "{$key}.")) {
                $isAllowed = true;
                break;
            }
        }

        $onlyByClass = $this->getFieldsByClassWithParents($class, $this->onlyByClass);

        if (!$isAllowed) {
            $shortKey = get_short_key($key);

            foreach ($onlyByClass as $field) {
                if ($shortKey === $field) {
                    $isAllowed = true;

                    break;
                }
            }
        }

        return (count($onlyByClass) == 0) || $isAllowed;
    }

    /**
     * @param string $target
     * @param array $specification
     * @return array
     */
    protected function getFieldsByClassWithParents(string $target, array $specification) : array
    {
        return array_reduce(array_keys($specification),
            function ($carry, $class) use ($target, $specification) {
                if (is_subclass_of($target, $class) || $class === $target) {
                    $carry = array_unique(array_merge($carry, $specification[$class]));
                }

                return $carry;
            }, []
        );
    }

    /**
     * @param object $object
     * @param ReflectionMethod $method
     * @return mixed
     */
    protected function getValue($object, ReflectionMethod $method)
    {
        return $method->invoke($object);
    }

    /**
     * @param string $key
     * @return string
     */
    protected function getFieldName(string $key) : string
    {
        return $this->getMappedName($this->scope->get(), $key);
    }

    /**
     * @param mixed $value
     * @param Root $root
     * @return mixed
     * @throws ExtractorException
     */
    protected function resolve($value, Root $root = null)
    {
        $scope = $this->scope->get();

        foreach ($this->resolvers as $resolver) {
            $canResolve = $resolver->canResolve($scope, $value, $root);

            if ($canResolve) {
                $resolver->log();
                return $resolver->resolve($scope, $value, $root);
            }
        }

        throw new ExtractorException("Can't extract object key '{$scope}'.");
    }

    /**
     * @param AbstractResolver $resolver
     * @param int $priority
     */
    public function addGlobalResolver(AbstractResolver $resolver, int $priority = 0) : void
    {
        $resolver->setExtractor($this);
        $this->resolvers->insert($resolver, $priority);
    }

    protected function createResolvers() : void
    {
        $this->addGlobalResolver(new SimpleResolver(), PHP_INT_MAX - 10);

        $resolvers = [
            SpecificationResolver::class,
            EnumCollectionResolver::class,
            CollectionResolver::class,
            EnumHashResolver::class,
            EnumResolver::class,
            DateTimeResolver::class,
            InstanceResolver::class
        ];

        foreach ($resolvers as $i => $resolver) {
            $this->addGlobalResolver($this->resolversContainer->get($resolver), 10 + ($i * 10));
        }
    }
}