<?php

namespace InscopeRest\Converter\Extractor;

class Root
{
	/**
	 * @var object
	 */
	private $object;

	/**
	 * @var string
	 */
	private $key;

	/**
	 * @param object $object
	 * @param string $key
	 */
	public function __construct($object, string $key)
	{
		$this->object = $object;
		$this->key = $key;
	}

	/**
	 * @return object
	 */
	public function getObject()
	{
		return $this->object;
	}

	/**
	 * @return string
	 */
	public function getKey() : string
	{
		return $this->key;
	}
}