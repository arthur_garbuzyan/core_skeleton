<?php

namespace InscopeRest\Converter\Support;

class Scope
{
    /**
     * @var array
     */
    private $data = [];

    private $depth = 100;

    /**
     * Creates scope
     *
     * @param string $key
     * @throws PossibleCircularReferenceException
     */
    public function create(string $key) : void
    {
        $this->data[] = $key;

        if (count($this->data) >= $this->depth) {
            $scope = $this->get();

            throw new PossibleCircularReferenceException(
                "Possible circular reference in key '{$scope}'");
        }
    }

    /**
     * Drops previously created scope
     */
    public function drop() : void
    {
        array_pop($this->data);
    }

    /**
     * Gets current scope or possible current scope if the key is provided
     *
     * @param string $key
     * @return string
     */
    public function get(string $key = null) : string
    {
        if ($key === null) {
            return $this->toString($this->data);
        }

        $data = $this->data;
        $data[] = $key;

        return $this->toString($data);
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool
    {
        return !$this->data;
    }

    /**
     * @param array $data
     * @return string
     */
    private function toString(array $data) : string
    {
        return implode('.', $data);
    }
}