<?php

namespace InscopeRest\Converter\Support;

trait DefaultTrait
{
    /**
     * @var array
     */
    protected $defaults = [];

    /**
     * Sets default values for empty field.
     * @param array $rules
     * @return self
     */
    public function defaults(array $rules) : self
    {
        $this->defaults = $rules;
        return $this;
    }
}