<?php

namespace InscopeRest\Converter\Support;

trait IgnoreTrait
{
    /**
     * Keeps config for ignoring
     *
     * @var array
     */
    protected $ignores = [];

    /**
     * Sets list ignored fields
     * @param array $fields
     * @return self
     */
    public function ignore(array $fields) : self
    {
        $this->ignores = array_merge($this->ignores, $fields);
        return $this;
    }

    /**
     * Return true if field in ignored list
     * @param string $field
     * @return bool
     */
    public function isIgnored(string $field) : bool
    {
        return in_array($field, $this->ignores);
    }
}