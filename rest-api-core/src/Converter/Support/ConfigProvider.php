<?php

namespace InscopeRest\Converter\Support;

class ConfigProvider
{
    private $config = [];

    public function __construct($config = [])
    {
        foreach (array_dot($config) as $key => $item) {
            $this->set($key, $item);
        }
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        return array_get($this->config, $key, $default);
    }

    /**
     * @param string $key
     * @param $value
     */
    public function set(string $key, $value) : void
    {
        array_set($this->config, $key, $value);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key) : bool
    {
        return array_has($this->config, $key);
    }
}