<?php

namespace InscopeRest\Converter\Support;

use InscopeRest\Modifier\Manager;

trait ModifyTrait
{
    /**
     * @var Manager
     */
    private $modifierManager;

    /**
     * Keeps config for modifiers
     *
     * @var array
     */
    private $modifiers = ['before' => [], 'after' => []];

    /**
     * Modifies value before resolution
     *
     * @param mixed $value
     * @param string $field
     * @return mixed
     */
    protected function modifyBefore($value, string $field)
    {
        return $this->modifyValue('before', $value, $field);
    }

    /**
     * Modifies value after resolution
     *
     * @param mixed $value
     * @param string $field
     * @return mixed
     */
    protected function modifyAfter($value, string $field)
    {
        return $this->modifyValue('after', $value, $field);
    }

    /**
     * @param string $block
     * @param mixed $value
     * @param string $field
     * @return mixed
     */
    protected function modifyValue(string $block, $value, string $field)
    {
        if (isset($this->modifiers[$block][$field])) {
            return $this->modifierManager->modify($value, $this->modifiers[$block][$field]);
        }

        return $value;
    }

    /**
     * @param string $type
     * @param string $field
     * @param mixed $modifiers
     * @return self
     */
    public function modify(string $type, string $field, $modifiers) : self
    {
        $this->modifiers[$type][$field] = $modifiers;
        return $this;
    }

    /**
     * @param Manager $manager
     * @return self
     */
    public function setModifierManager(Manager $manager) : self
    {
        $this->modifierManager = $manager;
        return $this;
    }

    /**
     * @return Manager
     */
    public function getModifierManager() : ?Manager
    {
        return $this->modifierManager;
    }
}