<?php

namespace InscopeRest\Converter\Support;

trait MapTrait
{
    /**
     * Keeps mapping
     *
     * @var array
     */
    protected $maps = [];

    protected $originalMaps = [];

    /**
     * @param array $rules
     * @return self
     */
    public function map(array $rules) : self
    {
        $this->originalMaps = array_merge($this->maps, $rules);
        $this->maps = array_merge($this->maps, $rules);

        return $this;
    }

    /**
     * Return mapped name if it exists in map
     * @param string $field
     * @param string $key
     * @return mixed
     */
    public function getMappedName(string $field, string $key = null)
    {
        if ($key === null) {
            $key = $field;
        }

        return array_take($this->maps, $field, $key);
    }
}