<?php

namespace InscopeRest\Converter\Transferer;

use InscopeRest\Converter\Extractor\Extractor;
use InscopeRest\Converter\Populator\Populator;
use InscopeRest\Converter\Support\DefaultTrait;
use InscopeRest\Converter\Support\IgnoreTrait;
use InscopeRest\Converter\Support\MapTrait;

class Transferer
{
    use IgnoreTrait;
    use MapTrait;
    use DefaultTrait;

    /**
     * @var array
     */
    protected $resolvers = [];

    /**
     * @var array
     */
    protected $nullable = [];

    /**
     * @var array
     */
    protected $hints = [];

    /**
     * @param array $config - can receive 'ignore', 'map', 'defaults' fields
     */
    public function __construct(array $config = [])
    {
        $this->ignore(array_take($config, 'ignore', []));
        $this->map(array_take($config, 'map', []));
        $this->defaults(array_take($config, 'defaults', []));
        $this->nullable(array_take($config, 'nullable', []));
        $this->resolvers = array_take($config, 'resolvers', []);
        $this->hints = array_take($config, 'hint', []);
    }

    /**
     * @param object $srcObject
     * @param object $destObject
     * @return object
     */
    public function transfer($srcObject, $destObject)
    {
        $data = $this->extract($srcObject);

        $flatten = array_smash($data, true);
        $transferredArray = dotted_array_merge($flatten, $this->getMappedValues($flatten));

        $transferredArray = array_filter($transferredArray, function($v, $k){
            return $v !== null || in_array($k, $this->nullable);
        }, ARRAY_FILTER_USE_BOTH);

        return $this->populate(array_unsmash($transferredArray), $destObject);
    }

    /**
     * @param $srcObject
     * @return array
     * @throws \InscopeRest\Converter\Extractor\ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    protected function extract($srcObject) : array
    {
        return (new Extractor(['ignore' => $this->ignores]))->extract($srcObject);
    }

    /**
     * @param array $data
     * @param $destObject
     * @return object
     * @throws \InscopeRest\Converter\Populator\PopulatorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    protected function populate(array $data, $destObject)
    {
        return (new Populator([
            'defaults' => $this->defaults,
            'resolver' => $this->resolvers,
            'hint' => $this->hints
        ]))->populate($data, $destObject);
    }

    /**
     * @param array $srcObject
     * @return array
     */
    private function getMappedValues(array $srcObject) : array
    {
        $data = [];

        foreach ($this->originalMaps as $key => $value) {
            if (array_key_exists($key, $srcObject)) {
                $data[$value] = $srcObject[$key];
            }
        }

        return $data;
    }

    /**
     * @param array $fields
     * @return self
     */
    public function nullable(array $fields) : self
    {
        $this->nullable = array_merge($this->nullable, $fields);
        return $this;
    }
}