<?php

namespace InscopeRest\Koala\Resource;

use InscopeRest\Koala\Error;
use InscopeRest\Koala\Metadata\MetadataProviderInterface;
use InscopeRest\Koala\Response\ResponseFactoryInterface;
use InscopeRest\Koala\Transformation\TransformableInterface;
use InscopeRest\Koala\Transformation\TransformCapableInterface;
use Illuminate\Http\Response;
use RuntimeException;
use Traversable;

class ResourceManager
{
    /**
     * @var TransformableInterface
     */
    private $transformer;

    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;

    /**
     * @var Error
     */
    private $error;

    /**
     * @param TransformableInterface $transformer
     * @param ResponseFactoryInterface $responseFactory
     * @param Error $error
     */
    public function __construct(
        TransformableInterface $transformer,
        ResponseFactoryInterface $responseFactory,
        Error $error
    ) {
        $this->transformer = $transformer;
        $this->responseFactory = $responseFactory;
        $this->error = $error;
    }

    /**
     * Creates a resource based on the provided item and the transformer handler
     *
     * @param object|array $item
     * @param TransformCapableInterface|callable $handler $handler
     * @return Response
     */
    public function make($item, $handler = null) : Response
    {
        if ($handler !== null){
            $item = $this->transformer->transform($item, $handler);
        }

        return $this->responseFactory->create($item, Response::HTTP_OK);
    }

    /**
     * Creates a collection of resources based on the provided items and the transformer handler
     *
     * @param iterable|array|Traversable $collection
     * @param TransformCapableInterface|callable $handler
     * @param MetadataProviderInterface $metadata
     * @return Response
     * @throws RuntimeException
     */
    public function makeAll(iterable $collection, $handler = null, MetadataProviderInterface $metadata = null) : Response
    {
        if ($handler !== null){
            $collection = $this->transformer->transformCollection($collection, $handler, $metadata);
        }

        return $this->responseFactory->create($collection, Response::HTTP_OK);
    }

    /**
     * Creates an empty resource
     *
     * @return Response
     */
    public function blank() : Response
    {
        return $this->responseFactory->create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Provides access to the error object to output http errors.
     *
     * @return Error
     */
    public function error() : Error
    {
        return $this->error;
    }
}