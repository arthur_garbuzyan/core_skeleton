<?php

namespace InscopeRest\Koala\Integration;

use InscopeRest\Koala\Pagination\PaginationInterface;
use League\Fractal\Pagination\PaginatorInterface;
use RuntimeException;

/**
 * The implementation of the Fractal's paginator interface in order to make the Transformer support pagination
 */
class Paginator implements PaginatorInterface
{
    /**
     * @var PaginationInterface
     */
    private $pagination;

    public function __construct(PaginationInterface $pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * Get the current page.
     *
     * @return int
     */
    public function getCurrentPage() : int
    {
        return $this->pagination->getCurrent();
    }

    /**
     * Get the last page.
     *
     * @return int
     */
    public function getLastPage() : int
    {
        return $this->pagination->getTotalPages();
    }

    /**
     * Get the total.
     *
     * @return int
     */
    public function getTotal() : int
    {
        return $this->pagination->getTotal();
    }

    /**
     * Get the number per page.
     *
     * @return int
     */
    public function getPerPage() : int
    {
        return $this->pagination->getPerPage();
    }

    /**
     * @return int
     * @throws RuntimeException
     */
    public function getCount() : int
    {
        return $this->pagination->getOnPage();
    }

    /**
     * @param int $page
     * @return string
     * @throws RuntimeException
     */
    public function getUrl($page) : string
    {
        return $this->pagination->generateUrl($page);
    }
}