<?php

namespace InscopeRest\Koala\Integration;

use App\Koala\Transformation\ReformatCapableInterface;
use InscopeRest\Koala\Config;
use InscopeRest\Koala\Metadata\MetadataProviderInterface;
use InscopeRest\Koala\Pagination\PaginationProviderInterface;
use InscopeRest\Koala\Transformation\TransformableInterface;
use InscopeRest\Koala\Transformation\TransformCapableInterface;
use ArrayIterator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\Serializer\ArraySerializer;
use Traversable;

class Transformer implements TransformableInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param $item
     * @param TransformCapableInterface|callable $handler
     * @return mixed
     */
    public function transform($item, $handler) : array
    {
        $manager = new Manager();

        $manager->setSerializer(new ArraySerializer());

        $wrapper = $this->config->get('transformer.wrapper.item');

        $resource = new Item($item, $handler, $wrapper);

        return $manager->createData($resource)->toArray();
    }

    /**
     * Transforms a collection with or without the pagination
     *
     * @param iterable $collection
     * @param TransformCapableInterface|callable $handler
     * @param MetadataProviderInterface $metadata
     * @return array|Traversable
     */
    public function transformCollection(iterable $collection, $handler, MetadataProviderInterface $metadata = null) : iterable
    {
        $manager = new Manager();

        $manager->setSerializer(new Serializer());

        $wrapper = $this->config->get('transformer.wrapper.collection');

        if ($handler instanceof ReformatCapableInterface){
            $collection = $handler->reformat($collection);
        }

        $resource = new Collection($this->prepareCollection($collection), $handler, $wrapper);

        if ($metadata){
            $this->setMetadata($resource, $metadata);
        }

        if ($collection instanceof PaginationProviderInterface) {
            $resource->setPaginator(new Paginator($collection->getPagination()));
        }

        return $manager->createData($resource)->toArray();
    }

    /**
     * @param ResourceAbstract $resource
     * @param MetadataProviderInterface|null $provider
     */
    private function setMetadata(ResourceAbstract $resource, MetadataProviderInterface $provider = null) : void
    {
        foreach ($provider->meta() as $key => $value) {
            $resource->setMetaValue($key, $value);
        }
    }

    /**
     * Converts the collection to the array or ArrayIterator if it is an instance of something else
     *
     * @param iterable $collection
     * @return array|ArrayIterator
     */
    private function prepareCollection(iterable $collection) : iterable
    {
        if (is_array($collection) || $collection instanceof ArrayIterator) {
            return $collection;
        }

        return iterator_to_array($collection);
    }
}