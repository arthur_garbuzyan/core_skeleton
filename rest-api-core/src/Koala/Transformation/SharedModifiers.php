<?php

namespace InscopeRest\Koala\Transformation;

use DateTime;
use RuntimeException;

class SharedModifiers
{
    /**
     * @var array
     */
    private $config;

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * Converts the provided date to the ISO8601 format
     *
     * @param DateTime $datetime
     * @return null|string
     */
    public function datetime(DateTime $datetime = null) : ?string
    {
        if ($datetime === null) {
            return null;
        }

        return $datetime->format(DateTime::ATOM);
    }


    /**
     * Converts the provided date to the ISO8601 format
     *
     * @param DateTime $datetime
     * @return null|string
     */
    public function timestamp(DateTime $datetime = null) : ?string
    {
        if ($datetime === null) {
            return null;
        }

        return $datetime->getTimestamp();
    }

    /**
     * @param object $object
     * @return string
     * @throws RuntimeException
     */
    public function stringable($object) : string
    {
        $map = array_take($this->config, 'stringable', []);

        foreach ($map as $class => $string){
            if ($object instanceof $class){
                return $string;
            }
        }

        throw new RuntimeException('Cannot convert instance of "'.get_class($object).'".');
    }
}
