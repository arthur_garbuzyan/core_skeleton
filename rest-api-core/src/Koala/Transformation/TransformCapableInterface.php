<?php

namespace InscopeRest\Koala\Transformation;

interface TransformCapableInterface
{
    /**
     * @param mixed $item
     * @return array
     */
    public function transform($item) : array;
}