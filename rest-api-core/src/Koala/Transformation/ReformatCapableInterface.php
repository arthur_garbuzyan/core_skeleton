<?php

namespace App\Koala\Transformation;

use Traversable;

interface ReformatCapableInterface
{
    /**
     * @param iterable|array|Traversable $traversable
     * @return iterable|array|Traversable
     */
    public function reformat(iterable $traversable) : iterable;
}