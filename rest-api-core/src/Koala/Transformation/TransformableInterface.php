<?php

namespace InscopeRest\Koala\Transformation;

use InscopeRest\Koala\Metadata\MetadataProviderInterface;
use Traversable;

interface TransformableInterface
{
    /**
     * Transforms a single item
     *
     * @param object $item
     * @param TransformCapableInterface|callable $handler
     * @return array
     */
    public function transform($item, $handler) : array;

    /**
     * Transforms a collection of items
     *
     * @param iterable|array|Traversable $collection
     * @param TransformCapableInterface|callable $handler
     * @param MetadataProviderInterface $metadata
     * @return array|Traversable
     */
    public function transformCollection(iterable $collection, $handler, MetadataProviderInterface $metadata = null) : iterable;
}