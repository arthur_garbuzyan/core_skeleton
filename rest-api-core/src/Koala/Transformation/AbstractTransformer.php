<?php

namespace InscopeRest\Koala\Transformation;

use InscopeRest\Converter\Extractor\Extractor;
use InscopeRest\Modifier\ModifierProviderTrait;
use League\Fractal\TransformerAbstract;

abstract class AbstractTransformer extends TransformerAbstract implements TransformCapableInterface
{
    use ModifierProviderTrait;

    /**
     * @var array
     */
    protected $options = [];

    /**
     * The shortcut method to extract data from the provided object
     *
     * @param $object
     * @param array $options
     * @return array
     * @throws \InscopeRest\Converter\Extractor\ExtractorException
     * @throws \InscopeRest\Converter\Support\PossibleCircularReferenceException
     * @throws \ReflectionException
     */
    protected function extract($object, array $options = []) : array
    {
        return $this->createExtractor($options)->extract($object);
    }

    /**
     * @param array $options
     * @return Extractor
     */
    protected function createExtractor(array $options = []) : Extractor
    {
        return (new Extractor(array_merge($this->options, $options)))
            ->setModifierManager($this->getModifierManager());
    }

    /**
     * @param string $field
     * @return bool
     */
    protected function isIncluded(string $field) : bool
    {
        return in_array($field, $this->getIncludes());
    }

    /**
     * @return array
     */
    public function getDefaults() : array
    {
        return array_take($this->options, 'onlyByClass', []);
    }

    /**
     * @param array $defaults
     * @return self
     */
    public function setDefaults(array $defaults)  : self
    {
        $this->options['onlyByClass'] = $defaults;
        return $this;
    }

    /**
     * @return array
     */
    public function getIncludes() : array
    {
        return array_take($this->options, 'only', []);
    }

    /**
     * @param array $includes
     * @return self
     */
    public function setIncludes(array $includes)  : self
    {
        $this->options['only'] = $includes;
        return $this;
    }

    /**
     * @return array
     */
    public function getIgnores() : array
    {
        return array_take($this->options, 'ignoreByClass', []);
    }

    /**
     * @param array $ignores
     * @return self
     */
    public function setIgnores(array $ignores)  : self
    {
        $this->options['ignoreByClass'] = $ignores;
        return $this;
    }

    /**
     * @param array $properties
     * @return self
     */
    public function setCalculatedProperties(array $properties) : self
    {
        $this->options['calculatedProperties'] = $properties;
        return $this;
    }

    /**
     * @param array $specifications
     * @return self
     */
    public function setSpecifications(array $specifications) : self
    {
        $this->options['specifications'] = $specifications;
        return $this;
    }

    /**
     * @param array $modifiers
     * @return self
     */
    public function setModifiers(array $modifiers) : self
    {
        $this->options['modifyByClass'] = $modifiers;
        return $this;
    }

    /**
     * @param callable $filter
     * @return self
     */
    public function setFilter(callable $filter) : self
    {
        $this->options['filterWithinContext'] = $filter;
        return $this;
    }
}