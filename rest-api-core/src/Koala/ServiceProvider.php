<?php

namespace InscopeRest\Koala;

use InscopeRest\Koala\Integration\Transformer;
use InscopeRest\Koala\Pagination\Paginator;
use InscopeRest\Koala\Response\JsonResponseFactory;
use InscopeRest\Koala\Response\ResponseFactoryInterface;
use InscopeRest\Koala\Transformation\TransformableInterface;
use Illuminate\Support\ServiceProvider as Provider;
use Illuminate\Contracts\Debug\ExceptionHandler as ExceptionHandlerInterface;

class ServiceProvider extends Provider
{
    public function register() : void
    {
        $this->app->singleton(TransformableInterface::class, Transformer::class);
        $this->app->singleton(ResponseFactoryInterface::class, JsonResponseFactory::class);
        $this->app->singleton(ExceptionHandlerInterface::class, ExceptionHandler::class);

        $this->app->singleton(Config::class, function ($app) {
            return new Config($app['config']->get('koala', []));
        });
    }

    public function boot() : void
    {
        Paginator::setConfig($this->app[Config::class]);
        Paginator::setRequest($this->app['request']);
    }
}