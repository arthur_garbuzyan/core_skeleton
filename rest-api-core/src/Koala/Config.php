<?php

namespace InscopeRest\Koala;

use ArrayObject;

class Config extends ArrayObject
{
    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     * Indicates whether the library is running in the debug mode
     *
     * @return bool
     */
    public function isDebug() : bool
    {
        return array_take($this, 'debug', false);
    }

    /**
     * Gets config by path
     *
     * @param string $path
     * @param null|mixed $default
     * @return mixed
     */
    public function get(string $path, $default = null)
    {
        return array_get($this->getArrayCopy(), $path, $default);
    }
}