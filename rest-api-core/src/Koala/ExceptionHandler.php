<?php

namespace InscopeRest\Koala;

use InscopeRest\Validation\ErrorsThrowableCollection;
use InscopeRest\Validation\PresentableException;
use Exception;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymphonyResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionHandler extends Handler
{
    /**
     * @var Error
     */
    private $error;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        PresentableException::class,
        ErrorsThrowableCollection::class
    ];

    /**
     * @param Container $container
     * @param Error $error
     * @param Config $config
     */
    public function __construct(Container $container, Error $error, Config $config)
    {
        parent::__construct($container);
        $this->error = $error;
        $this->config = $config;
    }

    /**
     * Renders an exception
     *
     * @param Request $request
     * @param Exception $e
     * @return SymphonyResponse
     */
    public function render($request, Exception $e) : SymphonyResponse
    {
        if ($e instanceof ErrorsThrowableCollection) {
            return $this->error->invalid($e);
        }

        if ($e instanceof PresentableException){
            return $this->error->write(Response::HTTP_BAD_REQUEST, $e->getMessage());
        }

        if ($e instanceof HttpException) {
            return $this->error->write($e->getStatusCode(), $e->getMessage());
        }

        if ($this->config->isDebug()) {
            return parent::render($request, $e);
        }

        return $this->error->internal();
    }
}
