<?php

namespace InscopeRest\Koala\Pagination;

/**
 * The interface is required to be implemented by a collection in order to provide the pagination details and tell the transformer to include them to the result.
 */
interface PaginationProviderInterface
{
    /**
     * @return PaginationInterface
     */
    public function getPagination() : PaginationInterface;
}