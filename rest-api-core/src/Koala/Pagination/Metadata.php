<?php

namespace InscopeRest\Koala\Pagination;

use RuntimeException;

/**
 * A simple implementation of the PaginationInterface to be used by Paginator
 */
class Metadata implements PaginationInterface
{
    /**
     * The current page
     * @var int
     */
    private $page;

    /**
     * The total number of items
     *
     * @var int
     */
    private $total;

    /**
     * The number of items per page
     *
     * @var int
     */
    private $perPage;

    /**
     * A callable callback to generate url
     *
     * @var callable
     */
    private $urlGenerator;

    /**
     * @param int $page
     * @param int $total
     * @param int $perPage
     */
    public function __construct(int $page, int $total, int $perPage)
    {
        $this->page = $page;
        $this->total = $total;
        $this->perPage = $perPage;
    }

    /**
     * Sets url generator
     *
     * @param callable $generator
     */
    public function setUrlGenerator(callable $generator) : void
    {
        $this->urlGenerator = $generator;
    }

    /**
     * Gets the current page
     * @return int
     */
    public function getCurrent() : int
    {
        return $this->page;
    }

    /**
     * Gets the total number of items
     *
     * @return int
     */
    public function getTotal() : int
    {
        return $this->total;
    }

    /**
     * Calculates total pages based on the total number of items and the number of items per page
     * @return int
     */
    public function getTotalPages() : int
    {
        return ceil($this->total / $this->perPage);
    }

    /**
     * Gets the number of items per page
     *
     * @return int
     */
    public function getPerPage() : int
    {
        return $this->perPage;
    }

    /**
     * Calculates number of items are shown on the page
     * @return int
     */
    public function getOnPage() : int
    {
        $all = $this->perPage * $this->page;

        if ($all <= $this->total) {
            return $this->perPage;
        }

        return $this->perPage - ($all - $this->total);
    }

    /**
     * Generates url based on provided page. Note: if the callable is not set the exception will be thrown
     * @param $page
     * @return string
     * @throws RuntimeException
     */
    public function generateUrl($page) : string
    {
        if ($this->urlGenerator === null) {
            throw new RuntimeException('Url generation is not supported');
        }

        return call_user_func($this->urlGenerator, $page);
    }
}