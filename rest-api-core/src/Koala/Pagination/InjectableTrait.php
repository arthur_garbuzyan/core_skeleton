<?php

namespace InscopeRest\Koala\Pagination;

use InscopeRest\Koala\Config;
use Illuminate\Http\Request;

/**
 * Trait is used to take out from the Paginator class the methods used to inject the thing from the environment.
 */
trait InjectableTrait
{
    /**
     * @var Request
     */
    private static $request;

    /**
     * @var Config
     */
    private static $config;

    /**
     * @param Request $request
     */
    public static function setRequest(Request $request) : void
    {
        static::$request = $request;
    }

    /**
     * @return Request
     */
    public static function getRequest() : Request
    {
        return static::$request;
    }

    /**
     * @param Config $config
     */
    public static function setConfig(Config $config) : void
    {
        static::$config = $config;
    }

    /**
     * @return Config
     */
    public static function getConfig() : Config
    {
        return static::$config;
    }
}
