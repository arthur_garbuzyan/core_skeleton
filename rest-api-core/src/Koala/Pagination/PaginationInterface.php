<?php

namespace InscopeRest\Koala\Pagination;

interface PaginationInterface
{
    /**
     * The current page
     *
     * @return int
     */
    public function getCurrent() : int;

    /**
     * Gets total items
     *
     * @return int
     */
    public function getTotal() : int;

    /**
     * Gets total pages
     *
     * @return int
     */
    public function getTotalPages() : int;

    /**
     * Gets number of items per page
     *
     * @return int
     */
    public function getPerPage() : int;

    /**
     * Get number of items on the current page
     *
     * @return int
     */
    public function getOnPage() : int;

    /**
     * Generates url based on the provided page number.
     *
     * @param $page
     * @return string
     */
    public function generateUrl($page) : string;
}
