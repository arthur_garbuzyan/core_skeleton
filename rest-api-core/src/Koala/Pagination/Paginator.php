<?php

namespace InscopeRest\Koala\Pagination;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use RuntimeException;
use Traversable;

/**
 * The class represents the collection of items that also provides pagination metadata.
 * Even though the class represents the collection of items it does not work with it directly.
 * Instead, the paginator must be provided with an adapter that in turn will provide
 * the collection and the total number of items through a common interface.
 */
class Paginator implements Countable, IteratorAggregate, PaginationProviderInterface
{
    use InjectableTrait;

    /**
     * A collection of items retrieved from the adapter
     *
     * @var array|Traversable
     */
    private $items;

    /**
     * The total number of items retrieved from the adapter
     * @var int
     */
    private $total;

    /**
     * Adapter that will provide actual items and the total number of items.
     *
     * @var AdapterInterface
     */
    private $adapter;

    /**
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Gets a collection of items thru the provided adapter
     *
     * @return array|Traversable
     */
    private function getItems() : iterable
    {
        if ($this->items === null) {

            $this->items = [];

            if ($this->getTotal() > 0) {
                $this->items = $this->adapter->getAll($this->getCurrentPage(), $this->getPerPage());
            }
        }

        return $this->items;
    }

    /**
     * Gets the total number of items thru the provided adapter
     *
     * @return int
     */
    private function getTotal() : int
    {
        if ($this->total === null) {
            $this->total = $this->adapter->getTotal();
        }

        return $this->total;
    }

    /**
     * Creates and return the pagination object that provides pagination metadata to the transformer
     *
     * @return PaginationInterface
     * @throws RuntimeException
     */
    public function getPagination() : PaginationInterface
    {
        $metadata = new Metadata($this->getCurrentPage(), $this->getTotal(), $this->getPerPage());

        $generator = static::getConfig()->get('pagination.urlGenerator');

        if ($generator) {
            $metadata->setUrlGenerator($generator);
        }
        return $metadata;
    }

    /**
     * Gets number of items per page from the request object or the default number
     *
     * @return int
     */
    private function getPerPage() : int
    {
        $default = static::getConfig()->get('pagination.perPage', 10);
        $num = static::getRequest()->input('perPage', $default);

        return (is_numeric($num) && $num > 0) ? (int)$num : $default;
    }

    /**
     * Gets the current page from the request object
     *
     * @return int
     */
    private function getCurrentPage() : int
    {
        $page = static::getRequest()->input('page', 1);

        return (is_numeric($page) && $page > 0) ? (int)$page : 1;
    }

    /**
     * Gets the collection iterator
     *
     * @return Traversable
     */
    public function getIterator() : iterable
    {
        $items = $this->getItems();

        if ($items instanceof Traversable) {
            return $items;
        }

        return new ArrayIterator($items);
    }

    /**
     * Counts number of items in the collection
     *
     * @return int
     */
    public function count() : int
    {
        return count($this->getItems());
    }
}