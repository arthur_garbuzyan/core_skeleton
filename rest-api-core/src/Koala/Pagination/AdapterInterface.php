<?php

namespace InscopeRest\Koala\Pagination;

/**
 * A common interface for all pagination adapters
 */
interface AdapterInterface
{
    /**
     * Gets all items according to the provided parameters
     *
     * @param int $page
     * @param int $perPage
     * @return iterable|object[]
     */
    public function getAll(int $page, int $perPage) : iterable;

    /**
     * Counts the total number of items
     *
     * @return int
     */
    public function getTotal() : int;
}