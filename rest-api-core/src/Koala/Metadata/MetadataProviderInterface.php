<?php

namespace InscopeRest\Koala\Metadata;

interface MetadataProviderInterface
{
    /**
     * @return array
     */
    public function meta() : array;
}