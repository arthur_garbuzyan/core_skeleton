<?php

namespace InscopeRest\Koala;

use InscopeRest\Koala\Response\ResponseFactoryInterface;
use InscopeRest\Validation\ErrorsThrowableCollection;
use Illuminate\Http\Response;
use InscopeRest\Validation\Error as ValidationError;

class Error
{
    /**
     * @var ResponseFactoryInterface
     */
    private $responseFactory;

    /**
     * Predefined http error messages.
     *
     * @var array
     */
    private $messages = [
        Response::HTTP_NOT_FOUND => 'Not Found',
        Response::HTTP_BAD_REQUEST => 'Bad Request',
        Response::HTTP_FORBIDDEN => 'Forbidden',
        Response::HTTP_UNAUTHORIZED => 'Unauthorized',
        Response::HTTP_INTERNAL_SERVER_ERROR => 'Internal Error'
    ];

    /**
     * @param ResponseFactoryInterface $responseFactory
     */
    public function __construct(ResponseFactoryInterface $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * Outputs the "internal server error" http error
     * @param null|string $message
     * @return Response
     */
    public function internal(string $message = null) : Response
    {
        return $this->write(Response::HTTP_INTERNAL_SERVER_ERROR, $message);
    }

    /**
     * @param string|null $message
     * @return Response
     */
    public function notFound(string $message = null) : Response
    {
        if (!$message){
            $message = $this->messages[Response::HTTP_NOT_FOUND];
        }

        return $this->write(Response::HTTP_NOT_FOUND, $message);
    }

    /**
     * @param ErrorsThrowableCollection $errors
     * @return Response
     */
    public function invalid(ErrorsThrowableCollection $errors) : Response
    {
        $data = [];

        /**
         * @var ValidationError[] $e
         */
        foreach ($errors as $property => $error){
            $data[$property] = $this->prepareError($error);
        }

        return $this->responseFactory->create(['errors' => $data], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param ValidationError $error
     * @return array
     */
    private function prepareError(ValidationError $error) : array
    {
        $data = [
            'identifier' => $error->getIdentifier(),
            'message' => $error->getMessage(),
            'extra' => []
        ];

        if ($error->hasExtra()){
            foreach ($error->getExtra() as $name => $extra){
                $data['extra'][$name] = $this->prepareError($extra);
            }
        }

        return $data;
    }

    /**
     * Outputs http error with the provided message and status code.
     * @param int $status
     * @param string $message
     * @return Response
     */
    public function write(int $status, string $message = null) : Response
    {
        if (!$message) {
            $message = array_take($this->messages, $status, 'Unknown Error');
        }

        $content = [
            'message' => $message,
            'code' => $status
        ];

        return $this->responseFactory->create($content, $status);
    }
}