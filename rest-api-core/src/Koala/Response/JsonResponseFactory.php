<?php

namespace InscopeRest\Koala\Response;

use Illuminate\Http\Response;

class JsonResponseFactory implements ResponseFactoryInterface
{
    /**
     * Creates a new json response object
     *
     * @param mixed $content
     * @param int $status
     * @return Response
     */
    public function create($content, int $status) : Response
    {
        return new Response(json_encode($content), $status, ['Content-Type' => 'application/json']);
    }
}