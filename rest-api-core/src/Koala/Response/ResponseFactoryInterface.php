<?php

namespace InscopeRest\Koala\Response;

use Illuminate\Http\Response;

interface ResponseFactoryInterface
{
    /**
     * Creates a response object
     *
     * @param $content
     * @param $status
     * @return Response
     */
    public function create($content, int $status) : Response;
}