<?php

namespace InscopeRest\Shared\Traits;

use Illuminate\Contracts\Container\Container;

trait ContainerAwareConstructorTrait
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }
}