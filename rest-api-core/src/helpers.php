<?php

/**
 * Takes the value from the array or returns default if the value is not set
 * @param iterable(array|ArrayAccess) $array
 * @param string $key
 * @param null $default
 * @return mixed
 * @throws RuntimeException
 */
function array_take(iterable $array, string $key, $default = null)
{

    if (!is_array($array) && !$array instanceof ArrayAccess) {
        throw new RuntimeException('$array must be array or instance of ArrayAccess');
    }

    return isset($array[$key]) ? $array[$key] : $default;
}

/**
 * @param array $data
 * @param callable $callback
 * @return array
 */
function array_define_keys(array $data, callable $callback) : array {
    $result = [];

    foreach ($data as $item){
        $key = $callback($item);
        if ($key === null) continue;
        $result[$callback($item)] = $item;
    }

    return $result;
}
/**
 * @param array $data
 * @param callable $callback
 * @return array
 */
function array_define_null_keys(array $data, callable $callback) : array {
    $result = [];

    foreach ($data as $item) {
        $key = $callback($item);
        if ($key !== null) continue;
        $result[] = $item;
    }

    return $result;
}

/**
 * Takes the value from the object or returns default if the value is not set
 * @param object $object
 * @param string $key
 * @param null $default
 * @return mixed|null
 * @throws RuntimeException
 */
function object_take($object, string $key, $default = null)
{
    if ($object === null) {
        return $default;
    }

    if (!is_object($object)) {
        throw new RuntimeException('$object must be object');
    }

    $keys = explode('.', $key);
    $currentKey = array_shift($keys);
    $getter = make_getter($currentKey);

    if (!method_exists($object, $getter)) {
        throw new  RuntimeException("Method '{$getter}' doesn't exists in object");
    }

    $value = call_user_func([$object, $getter]);

    if ((count($keys) > 0)) {
        if ($value === null) {
            return $default;
        }

        return object_take($value, implode('.', $keys), $default);
    }

    return $value;
}

/**
 * Get full key paths from multidimensional (or not as much) arrays
 * @param array $data
 * @param string $prepend
 * @return array
 */
function array_paths(array $data, string $prepend = '') : array
{
    $paths = [];

    foreach ($data as $key => $value) {
        $key = $prepend ? $prepend.'.'.$key : $key;

        $paths[] = $key;

        if (is_array($value)) {
            $paths = array_merge($paths, array_paths($value, $key));
        }
    }

    return $paths;
}

/**
 * Detect if a value is traversable
 * @param $array
 * @return bool
 */
function is_traversable($array) : bool
{
    return is_array($array) || $array instanceof Traversable;
}

/**
 * Detect if a value is array accessible
 *
 * @param $array
 * @return bool
 */
function is_arrayable($array) : bool
{
    return is_array($array) || $array instanceof ArrayAccess;
}

/**
 * Merge two dotted arrays
 * @param array $array1
 * @param array $array2
 * @return array
 */
function dotted_array_merge(array $array1, array $array2) : array
{
    foreach ($array2 as $key2 => $value2) {
        foreach (array_keys($array1) as $key1) {
            if (starts_with($key1, $key2)) {
                unset($array1[$key1]);
            }
        }

        $array1[$key2] = $value2;
    }

    return $array1;
}

/**
 * Flatten a multi-dimensional associative array with dots.
 * Unlike "array_dot", this  method stops flattening once it meets an array with numeric indexes starting with 0.
 *
 * @param  array $array
 * @param $vector - tells whether flattening should be stopped at vector.
 * @param  string $prepend
 * @return array
 */
function array_smash(array $array, bool $vector = false, string $prepend = '') : array
{
    return array_smash_callback($array, function ($key, $value) use ($vector) {
        return !($value && (!$vector || !is_vector($value, false)));
    }, $prepend);
}

/** Flatten a multi-dimensional associative array with dots.
 *  This method stops flattening for some keys for which $stopper return true.
 *
 * @param array $array
 * @param callable $stopper
 * @param string $prepend
 * @param bool|false $recursive - tells that even after smashing is stopped
 *            the resulting value will be tried to be smashed again before
 *            being assigned under the respective key in the resulting array.
 * @return array
 */
function array_smash_callback(array $array, callable $stopper, string $prepend = '', bool $recursive = false) : array
{
    $results = [];

    foreach ($array as $key => $value) {
        if (is_array($value) && !$stopper($key, $value)) {
            $results = array_merge($results, array_smash_callback($value, $stopper, $prepend . $key . '.'));
        } else {
            $results[$prepend . $key] = $recursive && is_array($value) ?
                array_smash_callback($value, $stopper, '', $recursive) : $value;
        }
    }

    return $results;
}

/**
 * Creates nested array from array with dot notation.
 *
 * @param array $dotted
 * @return array
 */
function array_unsmash($dotted) : array
{
    $array = [];

    foreach ($dotted as $key => $value) {
        array_set($array, $key, $value);
    }

    return $array;
}

/**
 * Checks if an array is vector
 *
 * @param $array
 * @param bool $scalar_only
 * @return bool
 */
function is_vector($array, bool $scalar_only = true) : bool
{
    if (!is_array($array)) {
        return false;
    }

    $next = 0;

    foreach ($array as $key => $value) {

        if ($scalar_only && !is_scalar($value) && $value !== null) {
            return false;
        }

        if ($key !== $next) {
            return false;
        }

        $next++;
    }

    return true;
}

/**
 * @param $array
 * @param int $sortFlags
 * @return array
 */
function ksort_recursive($array, $sortFlags = SORT_REGULAR) {
    if (!is_array($array)) {
        return $array;
    }
    ksort($array, $sortFlags);
    foreach ($array as &$arr) {
        $arr = ksort_recursive($arr, $sortFlags);
    }
    return $array;
}

/**
 * The function determines whether the method is getter
 *
 * @param string $method
 * @param array $types
 * @return bool
 */
function is_getter(string $method, array $types = ['get', 'is', 'has', 'can']) : bool
{
    $typesStr = implode('|', $types);
    return (bool)preg_match("/^({$typesStr})[A-Z0-9]/", $method);
}


/**
 * The function determines whether the method is setter
 *
 * @param string $method
 * @return bool
 */
function is_setter(string $method) : bool
{
    return (bool)preg_match('/^set[A-Z0-9]/', $method);
}


/**
 * The function resolves the property name from getter or setter
 *
 * @param $method
 * @param bool $real - if it is "true" the prefix "is" and "has" will be dropped
 * @return string
 * @throws RuntimeException
 */
function make_property(string $method, bool $real = false) : string
{
    $matches = [];

    if (!preg_match('/^(set|get|is|has|can)([A-Z0-9].*)/', $method, $matches)) {
        throw new RuntimeException('The method "' . $method . '" must be either getter or setter.');
    }

    if (!$real && in_array($matches[1], ['is', 'has', 'can'])) {
        return $method;
    }

    return lcfirst($matches[2]);
}

/**
 * Creates getter name from property name
 *
 * @param string $property
 * @return string
 * @throws RuntimeException
 */
function make_getter(string $property) : string
{
    if (preg_match('/^(is|has|can)[A-Z0-9]/', $property)) {
        return $property;
    }

    return 'get' . ucfirst($property);
}


/**
 * Creates setter name from property name
 *
 * @param string $property
 * @return string
 * @throws RuntimeException
 */
function make_setter(string $property) : string
{
    $matches = [];

    if (preg_match('/^(?:is|has|can)([A-Z0-9].*)/', $property, $matches)) {
        return 'set' . ucfirst($matches[1]);
    }

    return 'set' . ucfirst($property);
}

/**
 * Cuts off the specified substring from the beginning of the specified string
 *
 * @param string $string
 * @param string $unwanted
 * @return string
 */
function cut_string_left(string $string, string $unwanted) : string
{
    if (!starts_with($string, $unwanted)){
        return $string;
    }

    return substr($string, strlen($unwanted));
}

/**
 * Cuts off the specified substring from the end of the specified string
 *
 * @param string $string
 * @param string $unwanted
 * @return string
 */
function cut_string_right(string $string, string $unwanted) : string
{
    if (!ends_with($string, $unwanted)){
        return $string;
    }

    return substr($string, 0, strlen($string) - strlen($unwanted));
}

/**
 * Get last part from key with dot notation
 * @param string $key
 * @return string
 */
function get_short_key(string $key) : string
{
    return last(explode('.', $key));
}