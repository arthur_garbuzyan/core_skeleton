<?php

namespace InscopeRest\Validation;

use Countable;

class Value implements Countable
{
    /**
     * @var array
     */
    private $values = [];

    /**
     * @param mixed $value
     */
    public function addOptional($value) : void
    {
        $this->values[] = [$value, new Force(Force::OPTIONAL)];
    }

    /**
     * @param mixed $value
     */
    public function add($value) : void
    {
        $this->values[] = [$value, new Force(Force::REQUIRED)];
    }

    /**
     * @return bool
     */
    public function isNull() : bool
    {
        /**
         * @var Force $force
         */
        foreach ($this->values as list($value, $force)){
            if ($value === null && $force->is(Force::REQUIRED)){
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function first()
    {
        return $this->values[0][0];
    }

    /**
     * @return array
     */
    public function extract() : array
    {
        $result = [];

        foreach ($this->values as list($value)){
            $result[] = $value;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->values);
    }
}