<?php

namespace InscopeRest\Validation;

use InscopeRest\Enum\Enum;

class Force extends Enum
{
    const REQUIRED = 1;
    const OPTIONAL = 2;
}