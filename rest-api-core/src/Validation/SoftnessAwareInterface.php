<?php

namespace InscopeRest\Validation;

interface SoftnessAwareInterface
{
    /**
     * @param bool $flag
     */
    public function setSoft(bool $flag);

    /**
     * @return bool
     */
    public function isSoft() : bool;
}