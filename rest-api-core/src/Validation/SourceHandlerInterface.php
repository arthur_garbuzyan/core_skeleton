<?php

namespace InscopeRest\Validation;

interface SourceHandlerInterface
{
    /**
     * @return mixed
     */
    public function getSource();

    /**
     * @param string $property
     * @return mixed
     */
    public function getValue(string $property);

    /**
     * @param string $property
     * @return bool
     */
    public function hasProperty(string $property) : bool;
}