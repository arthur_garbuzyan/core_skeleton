<?php

namespace InscopeRest\Validation;

use RuntimeException;

class Property
{
    /**
     * @var RuleInterface[]
     */
    private $rules = [];

    /**
     * @param RuleInterface $rule
     * @return self
     * @throws RuntimeException
     */
    public function addRule(RuleInterface $rule) : self
    {
        $identifier = strtolower($rule->getIdentifier());

        if (isset($this->rules[$identifier])){
            throw new RuntimeException('Rule with the "'.$identifier.'" identifier has been already added.');
        }

        $this->rules[$identifier] = $rule;

        return $this;
    }

    /**
     * @return RuleInterface[]
     */
    public function getRules() : array
    {
        return $this->rules;
    }
}