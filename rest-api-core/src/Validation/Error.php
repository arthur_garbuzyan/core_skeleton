<?php

namespace InscopeRest\Validation;

use RuntimeException;

class Error
{
    /**
     * @var Error[]
     */
    private $extra;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $message;

    /**
     * @param string $identifier
     * @param string $message
     */
    public function __construct(string $identifier = null, string $message = null)
    {
        if ($identifier !== null) {
            $this->setIdentifier($identifier);
        }

        if ($message !== null) {
            $this->setMessage($message);
        }
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier(string $identifier) : void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string
     */
    public function getIdentifier() : string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getMessage() : string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message) : void
    {
        $this->message = $message;
    }

    /**
     * @param string $name
     * @param Error $error
     */
    public function addExtra(string $name, Error $error) : void
    {
        if (isset($this->extra[$name])) {
            throw new RuntimeException('The child with the "'.$name.'" name is already added.');
        }

        $this->extra[$name] = $error;
    }

    /**
     * @param string $name
     * @return Error|Error[]
     */
    public function getExtra(string $name = null)
    {
        if ($name) {
            return $this->extra[$name];
        }

        return $this->extra;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasExtra(string $name = null) : bool
    {
        if ($name){
            return isset($this->extra[$name]);
        }

        return count($this->extra ?? []) > 0;
    }
}