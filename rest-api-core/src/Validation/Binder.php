<?php

namespace InscopeRest\Validation;

use ArrayIterator;
use IteratorAggregate;

class Binder implements IteratorAggregate
{
    /**
     * @var array|callable[]
     */
    private $inflators = [];


    /**
     * @param string $name
     * @param callable|array $inflatorOrBundle
     * @param callable|null $inflator
     * @return Bundle
     */
    public function bind(string $name, $inflatorOrBundle, callable $inflator = null) : Bundle
    {
        if (is_callable($inflatorOrBundle) && $inflator === null) {
            $inflator = $inflatorOrBundle;
            $bundle = new Bundle($name, [$name]);
        } else{
            $bundle = new Bundle($name, $inflatorOrBundle);
        }

        $this->inflators[] = [$bundle, $inflator];

        return $bundle;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator() : ArrayIterator
    {
        return new ArrayIterator($this->inflators);
    }
}