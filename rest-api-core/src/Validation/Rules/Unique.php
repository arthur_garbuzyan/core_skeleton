<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use Traversable;

class Unique extends AbstractRule
{
    public function __construct()
    {
        $this->setIdentifier('unique');
        $this->setMessage('The values in the collection must be unique.');
    }

    /**
     * @param array|Traversable $collection
     * @return Error|null
     */
    public function check($collection) : ?Error
    {
        $values = [];

        foreach ($collection as $value) {
            if (in_array($value, $values)) {
                return $this->getError();
            }

            $values[] = $value;
        }

        return null;
    }
}