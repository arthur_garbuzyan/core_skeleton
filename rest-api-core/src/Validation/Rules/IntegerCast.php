<?php

namespace InscopeRest\Validation\Rules;

class IntegerCast extends Cast
{
    /**
     * @return string
     */
    protected function getType() : string
    {
        return 'integer';
    }

    /**
     * @param $value
     * @return bool
     */
    protected function softCheck($value) : bool
    {
        return (bool) filter_var($value, FILTER_VALIDATE_INT);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function hardCheck($value) : bool
    {
        return is_int($value);
    }
}