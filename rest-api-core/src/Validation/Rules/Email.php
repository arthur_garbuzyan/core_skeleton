<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Value;

class Email extends AbstractRule
{
    /**
     */
    public function __construct()
    {
        $this->setIdentifier('email');
        $this->setMessage('The value must be a valid email.');
    }

    /**
     * @param mixed|Value $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        if (! filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return $this->getError();
        }

        return null;
    }
}