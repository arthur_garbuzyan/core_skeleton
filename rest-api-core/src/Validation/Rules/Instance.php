<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Value;
use ReflectionClass;

class Instance extends AbstractRule
{
    /**
     * @var string
     */
    private $className;

    /**
     * @param string $className
     */
    public function __construct(string $className)
    {
        $this->className = $className;

        $this->setIdentifier('instance');
        $this->setMessage("The value must be an instance of ${className} class.");
    }


    /**
     * @param mixed|Value $value
     * @return Error|null
     * @throws \ReflectionException
     */
    public function check($value): ?Error
    {
        $class = new ReflectionClass($this->className);
        
        if (!$class->isInstance($value)) {
            return $this->getError();
        }

        return null;
    }
}