<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;

class Regex extends AbstractRule
{
    private $pattern;

    /**
     * @param string $pattern
     */
    public function __construct(string $pattern)
    {
        $this->pattern = $pattern;

        $this->setIdentifier('format');
        $this->setMessage('The value has incorrect format.');
    }

    /**
     * @param mixed $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        if (!is_string($value) && !is_int($value) && !is_float($value)) {
            return $this->getError();
        }

        $status = preg_match($this->pattern, $value);

        if (false === $status) {
            return $this->getError();
        }

        if (!$status) {
            return $this->getError();
        }

        return null;
    }
}