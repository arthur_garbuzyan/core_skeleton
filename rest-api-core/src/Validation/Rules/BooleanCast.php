<?php

namespace InscopeRest\Validation\Rules;

class BooleanCast extends Cast
{
    /**
     * @return string
     */
    protected function getType() : string
    {
        return 'boolean';
    }

    /**
     * @param $value
     * @return bool
     */
    protected function softCheck($value) : bool
    {
        $allowed = [true, false, 'true', 'false'];
        return in_array($value, $allowed, true);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function hardCheck($value) : bool
    {
        return is_bool($value);
    }
}