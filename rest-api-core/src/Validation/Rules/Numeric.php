<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;

class Numeric extends AbstractRule
{
    public function __construct()
    {
        $this->setMessage('The field must contain digits only.');
        $this->setIdentifier('numeric');
    }

    /**
     * @param mixed $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        $error = (new Regex('/^[0-9]+$/'))->check($value);

        if ($error){
            return $this->getError();
        }

        return null;
    }
}