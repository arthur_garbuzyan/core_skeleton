<?php

namespace InscopeRest\Validation\Rules;


use InscopeRest\Validation\Error;

abstract class Cast extends AbstractRule
{
    /**
     * @var bool
     */
    private $soft;

    public function __construct(bool $soft = false)
    {
        $this->soft = $soft;

        $this->setIdentifier('cast');
        $this->setMessage('The value must be '.$this->getType().'.');
    }

    /**
     * @param mixed $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        $valid = $this->soft ? $this->softCheck($value) : $this->hardCheck($value);

        if (!$valid){
            return $this->getError();
        }

        return null;
    }

    /**
     * @return string
     */
    protected abstract function getType() : string;

    /**
     * @param $value
     * @return bool
     */
    protected abstract function softCheck($value) : bool;


    /**
     * @param $value
     * @return bool
     */
    protected abstract function hardCheck($value) : bool;
}