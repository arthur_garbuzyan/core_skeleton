<?php

namespace InscopeRest\Validation\Rules;

class StringCast extends Cast
{
    public function __construct()
    {
        parent::__construct(false);
    }

    /**
     * @return string
     */
    protected function getType() : string
    {
        return 'string';
    }

    /**
     * @param $value
     * @return bool
     */
    protected function softCheck($value) : bool
    {
        return $this->hardCheck($value);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function hardCheck($value) : bool
    {
        return is_string($value);
    }
}