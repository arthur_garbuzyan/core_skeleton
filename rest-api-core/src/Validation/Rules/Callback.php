<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Value;

class Callback extends AbstractRule
{
    /**
     * @var callable
     */
    private $callback;

    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * @param mixed|Value $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        if (!call_user_func($this->callback, $value, $this)) {
            return $this->getError();
        }

        return null;
    }
}