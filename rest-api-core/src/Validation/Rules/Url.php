<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Value;

class Url extends AbstractRule
{
    public function __construct()
    {
        $this->setIdentifier('cast');
        $this->setMessage('The value must be a valid url.');
    }

    /**
     * @param mixed|Value $value
     * @return Error|null
     */
    public function check($value): ?Error
    {
        $value = filter_var($value, FILTER_SANITIZE_URL);

        if (!filter_var($value, FILTER_VALIDATE_URL)) {
            return $this->getError();
        }

        return null;
    }
}