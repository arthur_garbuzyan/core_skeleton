<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\RuleInterface;

abstract class AbstractRule implements RuleInterface
{
    /**
     * @var string
     */
    private $identifier;

    /**
     * @var Error
     */
    private $error;

    /**
     * @return string
     */
    public function getIdentifier() : string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return self
     */
    public function setIdentifier(string $identifier) : self
    {
        $this->identifier = $identifier;
        $this->getError()->setIdentifier($identifier);
        return $this;
    }

    /**
     * @param string $message
     * @return self
     */
    public function setMessage(string $message) : self
    {
        $this->getError()->setMessage($message);
        return $this;
    }

    /**
     * @return Error
     */
    protected function getError() : Error
    {
        if ($this->error === null){
            $this->error = new Error();
        }

        return $this->error;
    }
}