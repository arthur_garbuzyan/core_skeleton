<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Value;

class Alphanumeric extends AbstractRule
{
    public function __construct()
    {
        $this->setIdentifier('alphanumeric');
        $this->setMessage('The field must contain alphanumerics only.');
    }

    /**
     * @param mixed|Value $value
     * @return Error|null
     */
    public function check($value): ?Error
    {
        $error = (new Regex('/^[a-zA-Z0-9]+$/'))->check($value);

        if ($error){
            return $this->getError();
        }

        return null;
    }
}