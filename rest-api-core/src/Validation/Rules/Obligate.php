<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Value;

class Obligate extends AbstractRule implements NullProcessableRuleInterface
{
    /**
     * @param string $message
     */
    public function __construct(string $message = 'The field is required.')
    {
        $this->setIdentifier('required');
        $this->setMessage($message);
    }

    /**
     * @param mixed $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        if (($value instanceof Value && $value->isNull()) || $value === null){
            return $this->getError();
        }

        return null;
    }
}