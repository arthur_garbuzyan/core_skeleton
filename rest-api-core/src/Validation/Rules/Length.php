<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;

class Length extends AbstractRule
{
    /**
     * @var int
     */
    private $min;

    /**
     * @var int
     */
    private $max;

    /**
     * @var string
     */
    private $encoding;

    /**
     * @param int $min
     * @param int $max
     */
    public function __construct(int $min, int $max = null)
    {
        $this->min = $min;
        $this->max = $max;

        $postfix = '.';

        if ($max){
            $postfix = ' and less (or equal) than '.$max.'.';
        }

        $this->setMessage('The string length must be greater than (or equal) '.$min.$postfix);

        $this->setIdentifier('length');
    }

    /**
     * @param $encoding
     * @return self
     */
    public function setEncoding(string $encoding) : self
    {
        $this->encoding = $encoding;
        return $this;
    }

    /**
     * @param mixed $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        if ($this->encoding){
            $length = mb_strlen($value, $this->encoding);
        } else {
            $length = mb_strlen($value);
        }

        if ($length < $this->min){
            return $this->getError();
        }

        if ($this->max && $length > $this->max){
            return $this->getError();
        }

        return null;
    }
}