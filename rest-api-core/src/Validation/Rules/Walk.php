<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;

class Walk extends Each
{
    public function __construct(callable $inflator)
    {
        parent::__construct(function() use ($inflator) {
            return new Composite($inflator);
        });
    }

    /**
     * @param iterable $traversable
     * @return Error
     */
    protected function preCheck($traversable) : ?Error
    {
        $message = 'The collection must be an array where values must be either arrays or objects.';

        if ($error = parent::preCheck($traversable)) {
            return $this->setMessage($message)->getError();
        }

        $counter = 0;

        foreach ($traversable as $key => $value){
            if ($key != $counter || (!is_array($value) && !is_object($value))){
                return $this->setMessage($message)->getError();
            }

            $counter++;
        }

        return null;
    }
}