<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use InscopeRest\Validation\Value;
use DateTime;

class Moment extends AbstractRule
{
    /**
     * @var string
     */
    private $format;

    /**
     * @param string $format
     */
    public function __construct(string $format = DateTime::ATOM)
    {
        $this->format = $format;

        $this->setIdentifier('datetime');
        $this->setMessage('The value must be a valid datetime string.');
    }

    /**
     * @param mixed|Value $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        $datetime = new DateTime();

        $formatted = $datetime->createFromFormat($this->format, $value);

        if (!$formatted || $formatted->format($this->format) != $value)
        {
            return $this->getError();
        }

        return null;
    }
}