<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;

class Alpha extends AbstractRule
{
    /**
     * @var bool
     */
    private $withSpaces;

    /**
     * Alpha constructor.
     * @param bool $withSpaces
     */
    public function __construct(bool $withSpaces = false)
    {
        $this->withSpaces = $withSpaces;

        $this->setIdentifier('alpha');
        $this->setMessage('The field must contain letters only.');
    }

    /**
     * @param mixed $value
     * @return Error|null
     */
    public function check($value): ?Error
    {
        $error = (new Regex($this->regex()))->check($value);

        if ($error){
            return $this->getError();
        }

        return null;
    }

    /**
     * @return string
     */
    private function regex(): string
    {
        if ($this->withSpaces) {
            return '/^[a-zA-Z ]+$/';
        }

        return '/^[a-zA-Z]+$/';
    }
}