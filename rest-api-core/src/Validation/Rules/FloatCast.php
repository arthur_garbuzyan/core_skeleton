<?php

namespace InscopeRest\Validation\Rules;

class FloatCast extends Cast
{
    /**
     * @return string
     */
    protected function getType() : string
    {
        return 'float';
    }

    /**
     * @param $value
     * @return bool
     */
    protected function softCheck($value) : bool
    {
        return (bool) filter_var($value, FILTER_VALIDATE_FLOAT);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function hardCheck($value) : bool
    {
        return is_float($value) || is_int($value);
    }
}