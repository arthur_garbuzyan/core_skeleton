<?php

namespace InscopeRest\Validation\Rules;

use InscopeRest\Validation\Error;
use DateTime;

abstract class Compare extends AbstractRule
{
    /**
     * @var int|float|DateTime
     */
    private $numberOrDateTime;

    /**
     * @var bool
     */
    private $inclusive;

    /**
     * @param int|float|DateTime $numberOrDatetime
     * @param bool $inclusive
     */
    public function __construct($numberOrDatetime, bool $inclusive = true)
    {
        $this->numberOrDateTime = $numberOrDatetime;
        $this->inclusive = $inclusive;

        $this->setIdentifier($this->getDirection());

        $this->setMessage($this->prepareMessage($numberOrDatetime, $inclusive));
    }

    /**
     * @param int|float|DateTime $numberOrDatetime
     * @param bool|true $inclusive
     * @return string
     */
    private function prepareMessage($numberOrDatetime, bool $inclusive = true) : string
    {
        $inclusiveMessage = '';

        if ($inclusive){
            $inclusiveMessage = ' or equal';
        }

        $type = 'number';

        if ($numberOrDatetime instanceof DateTime){
            $numberOrDatetime = $numberOrDatetime->format('Y-m-d H:i:s');
            $type = 'datetime';
        }

        return 'The '.$type.' must be '.$this->getDirection().' than'.$inclusiveMessage.' "'.$numberOrDatetime.'".';
    }

    /**
     * @param mixed $value
     * @return Error|null
     */
    public function check($value) : ?Error
    {
        $compare = $this->compare($value, $this->numberOrDateTime);

        $valid = $this->inclusive ? ($compare || ($value == $this->numberOrDateTime)) : $compare;

        if (!$valid){
            return $this->getError();
        }

        return null;
    }

    /**
     * @param int|float|DateTime $value
     * @param int|float|DateTime $scope
     * @return bool
     */
    protected abstract function compare($value, $scope) : bool;

    /**
     * @return string
     */
    protected abstract function getDirection() : string;
}