<?php

namespace InscopeRest\Validation\Rules;

use DateTime;

class Less extends Compare
{
    /**
     * @param int|float|DateTime $value
     * @param int|float|DateTime $scope
     * @return bool
     */
    protected function compare($value, $scope) : bool
    {
        return $value < $scope;
    }

    /**
     * @return string
     */
    protected function getDirection() : string
    {
        return 'less';
    }
}