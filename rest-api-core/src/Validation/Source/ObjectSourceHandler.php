<?php

namespace InscopeRest\Validation\Source;

use InscopeRest\Validation\SourceHandlerInterface;

class ObjectSourceHandler implements SourceHandlerInterface
{
    /**
     * @var object
     */
    private $object;

    /**
     * @var array
     */
    private $forcedProperties;

    /**
     * @param object $object
     * @param array $forcedProperties
     */
    public function __construct($object, array $forcedProperties = [])
    {
        $this->object = $object;
        $this->forcedProperties = $forcedProperties;
    }

    /**
     * @return object
     */
    public function getSource()
    {
        return $this->object;
    }

    /**
     * @param string $property
     * @return mixed
     */
    public function getValue(string $property)
    {
        return object_take($this->object, $property);
    }

    /**
     * @param string $property
     * @return bool
     */
    public function hasProperty(string $property) : bool
    {
        $value = object_take($this->object, $property);

        return $value !== null || in_array($property, $this->forcedProperties);
    }

    /**
     * @param string $property
     */
    public function addForcedProperty(string $property) : void
    {
        $this->forcedProperties[] = $property;
    }

    /**
     * @return array
     */
    public function getForcedProperties() : array
    {
        return $this->forcedProperties;
    }
}