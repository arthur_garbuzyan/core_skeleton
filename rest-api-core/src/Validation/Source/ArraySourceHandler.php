<?php

namespace InscopeRest\Validation\Source;

use InscopeRest\Validation\SourceHandlerInterface;

class ArraySourceHandler implements SourceHandlerInterface
{
    /**
     * @var array
     */
    private $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getSource() : array
    {
        return $this->data;
    }

    /**
     * @param string $property
     * @return mixed
     */
    public function getValue(string $property)
    {
        return array_get($this->data, $property);
    }

    /**
     * @param string $property
     * @return bool
     */
    public function hasProperty(string $property) : bool
    {
        return array_has($this->data, $property);
    }
}
