<?php

namespace InscopeRest\Validation;

use ArrayIterator;
use Countable;
use IteratorAggregate;

class Bundle implements IteratorAggregate, Countable
{
    /**
     * @var array
     */
    private $properties = [];

    /**
     * @var string
     */
    private $name;

    /**
     * @var callable
     */
    private $constraint;

    /**
     * @var bool
     */
    private $ignoreSoftness = false;

    /**
     * @var callable
     */
    private $source;

    /**
     * @param string $name
     * @param array $properties
     */
    public function __construct(string $name, array $properties)
    {
        $this->name = $name;

        foreach ($properties as $key => $value) {
            if (is_int($key)) {
                $force = new Force(Force::REQUIRED);
                $property = $value;
            } else {
                $force = new Force($value);
                $property = $key;
            }

            $this->properties[] = [$property, $force];
        }
    }

    /**
     * @param callable $callback
     * @return self
     */
    public function when(callable $callback) : self
    {
        $this->constraint = $callback;
        return $this;
    }

    /**
     * @return callable
     */
    public function getConstraint() : ?callable
    {
        return $this->constraint;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator() : ArrayIterator
    {
        return new ArrayIterator($this->properties);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->properties);
    }

    /**
     * @param bool $flag
     */
    public function always(bool $flag)
    {
        $this->ignoreSoftness = $flag;
    }

    /**
     * @return bool
     */
    public function ignoreSoftness() : bool
    {
        return $this->ignoreSoftness;
    }

    /**
     * @param callable $callback
     * @return self
     */
    public function source(callable $callback) : self
    {
        $this->source = $callback;
        return $this;
    }

    /**
     * @return callable
     */
    public function getSource() : ?callable
    {
        return $this->source;
    }
}