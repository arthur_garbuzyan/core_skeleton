<?php

namespace InscopeRest\Validation;

use ArrayAccess;
use ArrayIterator;
use Countable;
use IteratorAggregate;
use RuntimeException;

class ErrorsThrowableCollection extends PresentableException implements IteratorAggregate, Countable, ArrayAccess
{
    /**
     * @var Error[]
     */
    private $errors = [];

    public function __construct()
    {
        parent::__construct('The data has not passed the validation.');
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator() : ArrayIterator
    {
        return new ArrayIterator($this->errors);
    }

    /**
     * @param string $property
     * @return bool
     */
    public function offsetExists($property) : bool
    {
        return isset($this->errors[$property]);
    }

    /**
     * @param string $property
     * @return Error
     */
    public function offsetGet($property) : Error
    {
        return $this->errors[$property];
    }

    /**
     * @param string $property
     * @param Error $error
     */
    public function offsetSet($property, $error) : void
    {
        if (!$error instanceof Error) {
            throw new RuntimeException('The error must be instance of the "Error" class.');
        }

        $this->errors[$property] = $error;
    }

    /**
     * @param string $property
     */
    public function offsetUnset($property) : void
    {
        unset($this->errors[$property]);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->errors);
    }

    /**
     * @param string $field
     * @param Error $error
     * @throws static
     */
    public static function throwError($field, Error $error) : void
    {
        $errors = new static();

        $errors[$field] = $error;

        throw $errors;
    }
}