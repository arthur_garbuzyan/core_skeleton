<?php

namespace InscopeRest\Validation;

interface RuleInterface
{
    /**
     * @param mixed|Value $value
     * @return Error|null
     */
    public function check($value) : ?Error;

    /**
     * @return string
     */
    public function getIdentifier() : string;
}