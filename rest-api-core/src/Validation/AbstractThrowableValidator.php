<?php

namespace InscopeRest\Validation;

use InscopeRest\Validation\Source\ArraySourceHandler;
use InscopeRest\Validation\Source\ObjectSourceHandler;

abstract class AbstractThrowableValidator
{
    private $forcedProperties = [];

    /**
     * @param Binder $binder
     * @return void
     */
    abstract protected function define(Binder $binder) : void;

    /**
     * @param object|array $source
     * @param bool $soft
     * @throws ErrorsThrowableCollection
     */
    public function validate($source, bool $soft = false) : void
    {
        $errors = (new Performer())->perform($this->getBinder(), $this->getSourceHandler($source), $soft);

        if (count($errors) > 0) {
            throw $errors;
        }
    }

    /**
     * @return Binder
     */
    private function getBinder() : Binder
    {
        $binder = new Binder();
        $this->define($binder);
        return $binder;
    }

    /**
     * @param array|object $source
     * @return SourceHandlerInterface
     */
    protected function getSourceHandler($source) : SourceHandlerInterface
    {
        if (is_array($source)){
            return new ArraySourceHandler($source);
        }

        return new ObjectSourceHandler($source, $this->forcedProperties);
    }

    /**
     * @param array $forcedProperties
     * @return self
     */
    public function setForcedProperties(array $forcedProperties) : self
    {
        $this->forcedProperties = $forcedProperties;
        return $this;
    }

    /**
     * @return array
     */
    public function getForcedProperties() : array
    {
        return $this->forcedProperties;
    }
}