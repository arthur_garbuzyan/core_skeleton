<?php

namespace InscopeRest\Routing;

use Illuminate\Contracts\Routing\Registrar;

interface RouteInterface
{
    public function register(Registrar $registrar) : void;
}