<?php

namespace InscopeRest\Modifier;

use RuntimeException;

class Manager
{
    /**
     * Contains all the registered modifiers
     * @var array
     */
    private $modifiers = [];

    /**
     * Keeps already resolved modifiers to improve performance
     * @var array
     */
    private $resolvedModifiers = [];

    /**
     * Register callable modifier
     * @param string $name
     * @param callable $callback
     * @throws RuntimeException
     */
    public function register(string $name, callable $callback) : void
    {
        $this->modifiers[] = [$name => $callback];
    }

    /**
     * Register object providing modifiers as public methods
     * @param object $provider
     * @throws RuntimeException
     */
    public function registerProvider($provider) : void
    {
        if (!is_object($provider)) {
            throw new RuntimeException('Modifiers provider must be an object');
        }

        $this->modifiers[] = $provider;
    }

    /**
     * Modifiers the value
     * @param $value
     * @param $modifiers
     * @return mixed
     * @throws RuntimeException
     */
    public function modify($value, $modifiers)
    {
        if (is_string($modifiers)) {
            $modifiers = explode('|', $modifiers);
        }

        foreach ($modifiers as $key => $modifier) {

            $params = [$value];

            if (!is_numeric($key)) {
                $params = array_merge($params, $modifier);
                $modifier = $key;
            }

            if (!isset($this->resolvedModifiers[$modifier])) {
                $callback = array_first($this->modifiers, function ($provider, $key) use ($modifier) {
                    if (is_object($provider)) {
                        return method_exists($provider, camel_case($modifier));
                    }
                    return isset($provider[$modifier]);
                });

                if ($callback === null) {
                    throw new RuntimeException('Tried to call the nonexistent modifier  "' . $modifier . '".');
                }

                if (is_object($callback)) {
                    $callback = [$callback, camel_case($modifier)];
                } else {
                    $callback = $callback[$modifier];
                }

                $this->resolvedModifiers[$modifier] = $callback;
            }

            $value = call_user_func_array($this->resolvedModifiers[$modifier], $params);
        }

        return $value;
    }

    /**
     * Creates a callable object to use as a function
     *
     * @return Callback|callable
     */
    public function make()
    {
        return new Callback($this);
    }
}