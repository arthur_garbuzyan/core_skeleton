<?php

namespace InscopeRest\Modifier;

class Callback
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @param Manager $manager
     */
    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $value
     * @param array|string $modifiers
     * @return mixed
     */
    public function __invoke(string $value, $modifiers)
    {
        return $this->manager->modify($value, $modifiers);
    }
}