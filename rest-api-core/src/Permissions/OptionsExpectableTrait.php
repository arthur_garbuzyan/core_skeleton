<?php

namespace InscopeRest\Permissions;

trait OptionsExpectableTrait
{
    /**
     * @var array
     */
    private $options;

    /**
     * @param array $options
     */
    public function setOptions(array $options) : void
    {
        $this->options = $options;
    }

    /**
     * @return array
     */
    protected function getOptions() : array
    {
        return $this->options;
    }
}