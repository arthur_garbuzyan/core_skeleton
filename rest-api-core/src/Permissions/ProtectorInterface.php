<?php

namespace InscopeRest\Permissions;

interface ProtectorInterface
{
    /**
     * @return bool
     */
    public function grants() : bool;
}