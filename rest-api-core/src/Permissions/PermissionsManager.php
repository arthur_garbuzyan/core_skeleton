<?php
namespace InscopeRest\Permissions;

use Illuminate\Contracts\Container\Container as ContainerInterface;
use RuntimeException;

/**
 * Provides access to the permissions layer within controllers
 */
class PermissionsManager
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var array
     */
    private $config;

    public function __construct(array $config, ContainerInterface $container)
    {
        $this->container = $container;
        $this->config = $config;
    }

    /**
     * @param string|array $protectors
     * @return bool
     * @throws RuntimeException
     */
    public function has($protectors) : bool
    {
        if (!is_array($protectors)) {
            $protectors = [$protectors];
        }

        foreach ($protectors as $protector) {
            $options = [];

            if (is_array($protector)){
                $options = array_take($protector, 1, []);
                $protector = $protector[0];
            }

            if (class_exists($protector)) {
                $class = $protector;
            } else {
                $class = array_get($this->config, 'protectors.'.$protector);
            }

            if (!$class) {
                throw new RuntimeException('Unable to retrieve class for the "'.$protector.'" protector.');
            }

            $instance = $this->container->make($class);

            if (!$instance instanceof ProtectorInterface){
                throw new RuntimeException('The instance of "'.$class.'" must be instance of ProtectorInterface.');
            }

            if ($instance instanceof OptionsExpectableInterface){
                $instance->setOptions($options);
            }

            if ($instance->grants()) {
                return true;
            }
        }

        return false;
    }
}