<?php

namespace InscopeRest\Permissions;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $options = $route->getAction();

        $details = $this->explode($options['uses']);

        $permissionsClass = $this->getPermissionsClass($details);

        if (!class_exists($permissionsClass)) {
            throw new PermissionsException('The permissions class "' . $permissionsClass . '" has not been found.');
        }

        $permissions = new $permissionsClass();

        if (!$permissions instanceof AbstractActionsPermissions) {
            throw new PermissionsException('The permissions class "' . $permissionsClass . '" must be instance of AbstractPermissions.');
        }

        /**
         * @var PermissionsManager $manager
         */
        $manager = app()->make(PermissionsManager::class);
        $protectors = $permissions->getProtectors($details['action']);

        if (!$manager->has($protectors)) {
            throw new AccessDeniedHttpException();
        }

        return $next($request);
    }

    /**
     * Prepares permissions object class
     *
     * @param array $details
     * @return string
     */
    private function getPermissionsClass(array $details) : string
    {
        return $details['controller']['packageNamespace'] . '\Permissions\\' . $details['controller']['name'] . 'Permissions';
    }

    /**
     * Prepares array with detailed information about the requested controller including the requested action name
     *
     * @param string $raw
     * @return array
     */
    private function explode($raw) : array
    {
        list($controller, $action) = explode('@', $raw);

        $class = substr($controller, strrpos($controller, '\\') + 1);
        $namespace = substr($controller, 0, strrpos($controller, '\\'));
        $packageNamespace = substr($namespace, 0, -1 * (strlen('controllers') + 1));
        $name = substr($class, 0, -1 * strlen('controller'));

        return [
            'action' => $action,
            'controller' => [
                'full' => $controller,
                'namespace' => $namespace,
                'packageNamespace' => $packageNamespace,
                'name' => $name,
                'class' => $class
            ]
        ];
    }
}