<?php

namespace InscopeRest\Permissions;

interface OptionsExpectableInterface
{
    /**
     * @param array $options
     * @return void
     */
    public function setOptions(array $options) : void;
}