<?php

namespace InscopeRest\Permissions;

use InscopeRest\Permissions\Protectors\AllProtector;
use InscopeRest\Permissions\Protectors\AuthProtector;
use Illuminate\Support\ServiceProvider as Provider;

class ServiceProvider extends Provider
{
    /**
     *
     */
    public function boot() : void
    {
        $this->app->resolving(
            PermissionsRequirableInterface::class,
            function ($controller) {
                $controller->middleware(Middleware::class);
            }
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() : void
    {
        $this->app->singleton(PermissionsManager::class, function($app) {
            return new PermissionsManager($app->make('config')->get('permissions', []), $app);
        });
    }
}