<?php

namespace InscopeRest\Permissions;

/**
 * Abstract class for all permissions classes.
 */
abstract class AbstractActionsPermissions
{
    /**
     * @return array
     */
    protected abstract function permissions() : array;

    /**
     * @param string $action
     * @return array|string
     */
    public function getProtectors(string $action)
    {
        return array_get($this->permissions(), $action, []);
    }
}