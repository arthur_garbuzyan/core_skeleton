<?php

namespace InscopeRest\Permissions\Protectors;

use InscopeRest\Permissions\ProtectorInterface;

class AllProtector implements ProtectorInterface
{
    /**
     * @return bool
     */
    public function grants() : bool
    {
        return true;
    }
}